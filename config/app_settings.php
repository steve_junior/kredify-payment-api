<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/11/2020
 * Time: 5:44 AM.
 */

return [
    /*
     |--------------------------------------------------------------------------
     | System Settings
     |
     | The values here binds the application to the country is it set up for.
     |--------------------------------------------------------------------------
     */

    'currency'          => env('CURRENCY', 'USD'),     //ISO 4217
    'country'           => env('COUNTRY', 'GHANA'),
    'countryCodeAlpha3' => env('COUNTRY_CODE_ALPHA_3', 'GHA'), //ISO 3166-1 alpha-3
    'countryCodeAlpha2' => env('COUNTRY_CODE_ALPHA_2', 'GH'),  //ISO 3166-1 alpha-2

    'numberOfPayouts'   => env('NUMBER_OF_PAYOUTS', 4),
    'numberOfDaysPerPayouts'   => env('NUMBER_OF_DAYS_PER_PAYOUTS', 14),
    'ratioOfPayoutsLowRisk'    => env('RATIO_OF_PAYOUTS_LOW_RISK'),
    'ratioOfPayoutsNormalRisk' => env('RATIO_OF_PAYOUTS_NORMAL_RISK'),
    'ratioOfPayoutsHighRisk'   => env('RATIO_OF_PAYOUTS_HIGH_RISK'),
    'ratioOfPayoutsVeryHighRisk'   => env('RATIO_OF_PAYOUTS_VERY_HIGH_RISK'),

    'whitelistedIps' => explode(',', env('WHITELISTED_IPS', '127.0.0.1')),
];
