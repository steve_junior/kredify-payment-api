<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::group(['prefix' => 'checkout'], function () {
        Route::get('schedules', [App\Http\Controllers\Checkout\ScheduleController::class, 'getSchedules'])
            ->name('checkout.schedules');

        Route::get('payment-url', [App\Http\Controllers\Checkout\PaymentController::class, 'getPaymentURL']);
    });

    Route::get('get-user', function (Request $request) {
        return $request->user();
    })->prefix('v1');
});
