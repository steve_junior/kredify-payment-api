<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//Auth Routes
Route::group(['prefix' => 'auth'], function () {
    Route::post('email/scan', [App\Http\Controllers\Authentication\ScanEmailController::class, 'action']);
    Route::post('phone-number', [App\Http\Controllers\Authentication\RegisterPhoneNumberController::class, 'addPhoneNumber']);
    Route::post('phone-number/verify', [App\Http\Controllers\Authentication\RegisterPhoneNumberController::class, 'verifyPhoneNumber']);
    Route::post('password', [App\Http\Controllers\Authentication\RegisterPasswordController::class, 'action']);
    Route::post('address', [App\Http\Controllers\Authentication\RegisterNameAndAddressController::class, 'action']);

    Route::post('login', [App\Http\Controllers\Authentication\LoginController::class, 'login'])->name('login');
    Route::post('logout', [App\Http\Controllers\Authentication\LoginController::class, 'logout'])->name('logout');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [App\Http\Controllers\TestController::class, 'test']);

/*
 * Place callbacks & webhook here and add the relative path to the except array of VerifyCSRF class
 */
Route::get('paystack/callback', [App\Http\Controllers\Checkout\CallbackController::class, 'paystack'])->name('paystack.callback');
