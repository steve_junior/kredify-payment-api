<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 8:32 AM.
 */

use Illuminate\Support\Facades\Route;

Route::get('ping', [\App\Http\Controllers\ServiceController::class, 'ping'])->middleware('acceptable.origin');
Route::get('payment-providers', [\App\Http\Controllers\TestController::class, 'getPaymentProvider'])->middleware('acceptable.origin');
