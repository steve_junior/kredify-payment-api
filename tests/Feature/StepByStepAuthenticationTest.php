<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\FeatureCommoners;
use App\Models\Consumer\Consumer;
use Illuminate\Testing\TestResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StepByStepAuthenticationTest extends FeatureCommoners
{
    use RefreshDatabase;

    private static $AUTH_PREFIX = 'auth/';

    protected $csrfToken;

    protected $registrationId;

    public function testAuthentication()
    {
        //test sanctum CSRF Exists
        $this->getSanctumCSRFTCookie();

        // test email scan
        $this->emailScanNewConsumer();

        //test phone number
        $this->addPhoneNumber();

        //test verify phone number
        $this->verifyPhoneNumber();

        //test add password
        $this->addPassword();

        //test add name & address
        $this->addNameAndAddresses();

        //test login
        $this->login();

        //test login
        $this->logout();
    }

    private function getSanctumCSRFTCookie()
    {
        $response = $this->get('/sanctum/csrf-cookie');

        $response->assertCookie('XSRF-TOKEN');

        $response->assertNoContent();

        $this->csrfToken = $response->cookie('XSRF-TOKEN');
    }

    private function emailScanNewConsumer()
    {
        $response = $this->createPost(
            self::$AUTH_PREFIX.'email/scan',
            ['email' => self::$EMAIL],
            $this->getCSRFHeader($this->csrfToken)
        );

        $this->checkResponseJsonStructure($response);

        $response->assertCreated();

        $response->assertJson([
            'data'  =>  [
                'isNew'           => true,
                'defaultLang'     => config(CONFIG_APP_LOCALE_LANG),
                'defaultCurrency' => config(CONFIG_SETTINGS_CURRENCY),
                'hasPassword'     => false,
                'hasPhoneNumber'  => false,
                'hasPhoneNumberVerified' => false,
                'hasLegalName'    => false,
                'hasAddress'      => false,
            ],
            'error' => [
                'status' => 'success',
                'code'   => 201,
                'msg'    => 'Consumer created',
                'data'   => [],
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'regId',
            ],
        ]);

        $this->registrationId = $this->getRegId();
    }

    private function addPhoneNumber()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'phone-number',
            [
                'registrationId' => $this->registrationId,
                'phoneNumber'    => self::$PHONE_NUMBER,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'  =>  [
                'hasPhoneNumber'  => true,
                'hasPhoneNumberVerified' => false,
                'smsSent'         => true,
                'countryCode'     => config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2),
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'regId',
            ],
        ]);
    }

    private function verifyPhoneNumber()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'phone-number/verify',
            [
                'verificationCode' => self::$VERIFICATION_CODE,
                'registrationId'   => $this->registrationId,
                'phoneNumber'      => self::$PHONE_NUMBER,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                'regId',
            ],
        ]);

        $response->assertJson([
            'data'  =>  [
                'hasPhoneNumber'  => true,
                'hasPhoneNumberVerified' => true,
                'smsSent'         => true,
                'countryCode'     => config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2),
            ],
        ]);

        $response->assertJsonStructure([
            'data' => [
                'regId',
            ],
        ]);
    }

    private function addPassword()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'password',
            [
                'registrationId' => $this->registrationId,
                'password'       => self::$PASSWORD,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'  =>  [
                'hasPassword'         => true,
                'defaultCurrency'     => config(CONFIG_SETTINGS_CURRENCY),
                'defaultLang'         => config(CONFIG_APP_LOCALE_LANG),
            ],
        ]);
    }

    private function addNameAndAddresses()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'address',
            [
                'registrationId' => $this->registrationId,
                'legalFirstName' => self::$FIRST_NAME,
                'legalLastName'  => self::$LAST_NAME,
                'region'         => self::$REGION,
                'gMapAddress'    => self::$G_ADDRESS,
                'landmark'       => self::$LANDMARK,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'  =>  [
                'hasLegalName'        => true,
                'hasAddress'          => true,
                'defaultCurrency'     => config(CONFIG_SETTINGS_CURRENCY),
                'defaultLang'         => config(CONFIG_APP_LOCALE_LANG),
            ],
        ]);
    }

    private function login()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'login',
            [
                'email'    => self::$EMAIL,
                'password' => self::$PASSWORD,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'   => [],
            'error'  =>  [
                'status' => 'success',
                'msg'    => 'Authenticated',
                'code'   => 200,
            ],
        ]);
    }

    private function logout()
    {
        $response = $this->createPost(self::$AUTH_PREFIX.'logout',
            [], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'   => [],
            'error'  =>  [
                'status' => 'success',
                'msg'    => 'Exited',
                'code'   => 200,
            ],
        ]);
    }
}
