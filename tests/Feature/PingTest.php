<?php

namespace Tests\Feature;

use Tests\FeatureCommoners;

class PingTest extends FeatureCommoners
{
    protected $successResponse = [
        'data'  =>  'pong',
        'error' => [
            'status' => 'success',
            'code'   => 200,
            'msg'    => 'Server is ready to accept requests',
            'data'   => [],
        ],
    ];

    /**
     * @return void
     */
    public function testSuccessfulPing()
    {
        $response = $this->createGet('/ping', $this->getRefererHeader());

        $response->assertStatus(200);
        $response->assertJson($this->successResponse);
    }
}
