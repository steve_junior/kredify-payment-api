<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 04/01/2021
 * Time: 3:15 PM.
 */

namespace Tests\Feature;

use Tests\FeatureCommoners;
use App\Models\Consumer\Consumer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SingleStepAuthenticationTest extends FeatureCommoners
{
    use RefreshDatabase;

    protected $csrfToken;

    public function testAuthentication()
    {
        $this->createUser();

        //test sanctum CSRF Exists
        $this->getSanctumCSRFTCookie();

        // test email scan
        $this->emailScanExistingConsumer();

        //test login
        $this->login();
    }

    private function createUser()
    {
        $consumer = Consumer::create([
            'email' => self::$EMAIL, 'phone_number' => self::$PHONE_NUMBER,
        ]);

        $consumer->first_name = self::$FIRST_NAME;
        $consumer->last_name = self::$LAST_NAME;
        $consumer->is_phone_number_verified = true;
        $consumer->phone_number_verified_at = now();
        $consumer->phone_number_verification_code = self::$VERIFICATION_CODE;
        $consumer->residential_address_google_maps = self::$G_ADDRESS;
        $consumer->region = self::$REGION;
        $consumer->landmarks = self::$LANDMARK;
        $consumer->sms_code_verify_phone_delivered = true;
        $consumer->sms_code_verify_phone_delivered_at = now()->subMinute();
        $consumer->password = Hash::make(self::$PASSWORD);

        $consumer->save();
    }

    private function getSanctumCSRFTCookie()
    {
        $response = $this->get('/sanctum/csrf-cookie');

        $response->assertCookie('XSRF-TOKEN');

        $response->assertNoContent();

        $this->csrfToken = $response->cookie('XSRF-TOKEN');
    }

    private function emailScanExistingConsumer()
    {
        $response = $this->createPost(
             'auth/email/scan',
            ['email' => self::$EMAIL],
            $this->getCSRFHeader($this->csrfToken)
        );

        $this->checkResponseJsonStructure($response);

        $response->assertOk();

        $response->assertJson([
            'data'  =>  [
                'isNew'           => false,
                'defaultLang'     => config(CONFIG_APP_LOCALE_LANG),
                'defaultCurrency' => config(CONFIG_SETTINGS_CURRENCY),
                'hasPassword'     => true,
                'hasPhoneNumber'  => true,
                'hasPhoneNumberVerified' => true,
                'hasLegalName'    => true,
                'hasAddress'      => true,
            ],
            'error' => [
                'status' => 'success',
                'code'   => 200,
                'msg'    => 'Consumer retrieved',
                'data'   => [],
            ],
        ]);
    }

    private function login()
    {
        $response = $this->createPost('auth/login',
            [
                'email'    => self::$EMAIL,
                'password' => self::$PASSWORD,
            ], $this->getCSRFHeader($this->csrfToken));

        $this->checkResponseJsonStructure($response);

        $response->assertStatus(200);

        $response->assertJson([
            'data'   => [],
            'error'  =>  [
                'status' => 'success',
                'msg'    => 'Authenticated',
                'code'   => 200,
            ],
        ]);
    }
}
