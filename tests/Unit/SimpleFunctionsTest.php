<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class SimpleFunctionsTest extends TestCase
{
    /**
     * A unit test for is_true($value).
     *
     * @return void
     */
    public function testIsTrue()
    {
        $this->assertFalse(is_true('0'));
        $this->assertTrue(is_true('1'));
        $this->assertFalse(is_true(null));
        $this->assertFalse(is_true([]));
        $this->assertFalse(is_true(init()));
    }
}
