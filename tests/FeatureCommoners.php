<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/01/2021
 * Time: 5:45 PM.
 */

namespace Tests;

use Illuminate\Testing\TestResponse;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeatureCommoners extends TestCase
{
    protected static $EMAIL = 'test@kredify.net';

    protected static $PASSWORD = 'password';

    protected static $PHONE_NUMBER = '0201234567';

    protected static $VERIFICATION_CODE = '1111';

    protected static $FIRST_NAME = 'Alex';

    protected static $LAST_NAME = 'Krugiel';

    protected static $REGION = 'Greater Accra';

    protected static $LANDMARK = 'Bank of Ghana';

    protected static $G_ADDRESS = 'Essefo Street, Asylum Down Accra';

    protected static function getResponseJsonStructure()
    {
        return [
            'data',
            'error' => ['status', 'code', 'msg', 'data'],
        ];
    }

    protected function getCSRFHeader($token)
    {
        return ['X-XSRF-TOKEN' => $token];
    }

    protected function getRefererHeader()
    {
        return ['Referer' => 'localhost'];
    }

    protected function getDefaultHeaders()
    {
        return ['Accept' => 'application/json', 'Content-Type' => 'application/json'];
    }

    protected function getHeaders(array $headers = [])
    {
        return array_merge($headers, $this->getDefaultHeaders());
    }

    protected function createGet($relative_path, array $headers = [])
    {
        $http_headers = $this->getHeaders($headers);

        return $this->get($relative_path, $http_headers);
    }

    protected function createPost($relative_path, array $data, array $headers = [])
    {
        $http_headers = $this->getHeaders($headers);

        return $this->postJson($relative_path, $data, $http_headers);
    }

    protected function getRegId()
    {
        $consumer = \App\Models\Consumer\Consumer::all()->firstWhere('email', self::$EMAIL);

        $registrationId = '';

        if ($consumer instanceof \App\Models\Consumer\Consumer) {
            $registrationId = $consumer->registration_id;
        }

        return $registrationId;
    }

    protected function checkResponseJsonStructure(TestResponse $response)
    {
        $response->assertJsonStructure(self::getResponseJsonStructure());
    }
}
