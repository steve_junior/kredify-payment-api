<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 02/12/2020
 * Time: 4:47 AM.
 */

namespace App\Repositories;

use InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     *  Get a one instance that matches the search query.
     *  By default it get the first model.
     *
     * @param  array $parameters
     * @param  array $options
     * @param  $model
     *
     * @return Model model
     */
    public function _findOne($parameters, $options, $model)
    {
        $result = $this->_find($parameters, $model);
        $model = null;

        if (is_array($options)) {
            if (empty($options) || $options['position'] == 'first') {
                $model = $result->first();
            } elseif ($options['position'] == 'last') {
                $model = $result->last();
            }
        } else {
            throw new InvalidArgumentException('Options can only array');
        }

        return $model;
    }

    /**
     *  Get a collection that matches the search query.
     *
     * @param  array $parameters
     * @param  Model $model
     * @return \Illuminate\Database\Eloquent\Builder[]|
     * \Illuminate\Database\Eloquent\Collection
     */
    public function _find($parameters, $model)
    {
        $models = $model::query();

        foreach ($parameters as $key => $value) {
            $models->where($key, $value);
        }

        return $models->get();
    }

    /**
     *  Finds models matching the query & update with values.
     *
     * @param  array $parameters
     * @param  array $newValues
     * @param  $model
     * @return bool
     */
    public function _findAndUpdate($parameters, array $newValues, $model)
    {
        $modelInstance = $this->_findOne($parameters, [], $model);

        if ($modelInstance instanceof $model) {
            return $modelInstance->updateAttributes($newValues);
        }

        return false;
    }
}
