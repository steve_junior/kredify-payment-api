<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 05/12/2020
 * Time: 2:27 AM.
 */

namespace App\Repositories;

use App\Models\Consumer\RiskProfile;

class RiskProfileRepository extends BaseRepository
{
    public function findOne($parameters = [], $options = [])
    {
        return parent::_findOne($parameters, $options, RiskProfile::class);
    }
}
