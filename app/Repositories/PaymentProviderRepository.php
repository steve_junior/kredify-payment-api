<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/12/2020
 * Time: 6:04 AM.
 */

namespace App\Repositories;

use App\Models\Payments\Provider as PaymentProvider;

class PaymentProviderRepository extends BaseRepository
{
    /**
     *  Get an instance of the default payment provider.
     *
     * @return PaymentProvider
     */
    public function getDefault() : PaymentProvider
    {
        return PaymentProvider::all()->first();
    }

    public function findOne($parameters = [], $options = [])
    {
        return parent::_findOne($parameters, $options, PaymentProvider::class);
    }
}
