<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 21/07/2020
 * Time: 6:03 AM.
 */

namespace App\Repositories;

use App\Models\Checkout;

class CheckoutRepository extends BaseRepository
{
    public function findOne($parameters = [], $options = [])
    {
        return parent::_findOne($parameters, $options, Checkout::class);
    }

    public function find($parameters = [])
    {
        return parent::_find($parameters, Checkout::class);
    }

    public function findAndUpdate($parameters = [], array $newValues = [])
    {
        return parent::_findAndUpdate($parameters, $newValues, Checkout::class);
    }
}
