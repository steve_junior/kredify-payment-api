<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 10:21 AM.
 */

namespace App\Repositories;

use App\Models\Consumer\Consumer;

class ConsumerRepository extends BaseRepository
{
    public function create(array $data)
    {
        return Consumer::create($data);
    }

    public function one()
    {
        return Consumer::all()->first();
    }

    public function getConsumerByEmail($email)
    {
        return Consumer::all()->firstWhere('email', $email);
    }

    public function getConsumerByRegistrationId($regId)
    {
        return Consumer::all()->firstWhere('registration_id', $regId);
    }

    public function getUpdatedConsumer(Consumer $consumer, array $attributes)
    {
        foreach ($attributes as $key => $attribute) {
            $consumer->{$key} = $attribute;
        }

        $consumer->save();

        return $consumer;
    }
}
