<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/11/2020
 * Time: 1:04 PM.
 */

namespace App\Repositories;

use InvalidArgumentException;
use App\Models\Payments\Schedule as PaymentSchedule;

class PaymentScheduleRepository
{
    /**
     *  Get a one instance that matches the search query.
     *  By default it get the first model.
     *
     * @param  array $parameters
     * @param  array $options
     * @return PaymentSchedule model
     */
    public function findOne($parameters = [], $options = [])
    {
        $result = $this->find($parameters);
        $model = null;

        if (is_array($options)) {
            if (empty($options) || $options['position'] == 'first') {
                $model = $result->first();
            } elseif ($options['position'] == 'last') {
                $model = $result->last();
            }
        } else {
            throw new InvalidArgumentException('Options can only array');
        }

        return $model;
    }

    /**
     *  Get a collection that matches the search query.
     *
     * @param  array $parameters
     * @return \Illuminate\Database\Eloquent\Builder[]|
     * \Illuminate\Database\Eloquent\Collection
     */
    public function find($parameters = [])
    {
        $schedules = PaymentSchedule::query();

        foreach ($parameters as $key => $value) {
            $schedules->where($key, $value);
        }

        return $schedules->get();
    }
}
