<?php

namespace App\Listeners;

use App\Services\LogService;
use App\Models\Consumer\Consumer;
use App\Models\Payments\Provider;
use App\Events\PaidThroughPayStack;
use App\Repositories\ConsumerRepository;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\PaymentProviderRepository;
use App\Models\Payments\Method as PaymentMethod;

/**
 * Keeps a copy of the payment method used by the consumer
 * Sets the most recent payment method to default which will used
 * for auto-debiting.
 *
 *
 *  Payment Method is simply data that give
 *  Kredify authorisation to auto charge a consumer
 */
class UpdateConsumerPaymentMethod
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        //http://consumer.kredify.local:8000/paystack/callback?trxref=160608022868&reference=160608022868

        try {
            $consumer = $this->getConsumer($event);
            $payment = $this->getPaymentDetails($event->payment->response, $this->getProvider($event), $consumer);

            if ($consumer->paymentMethods->isEmpty()) {
                // consumer does not have any payment method saved to their account.
                $this->createConsumerPaymentMethod($payment, true);
            } else {
                //only add to the consumer payment methods if its new otherwise update
                $this->updateOrCreateConsumerPaymentMethod($payment, $consumer);
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_UpdateConsumerPaymentMethodListenerError, $exception);
        }
    }

    /**
     * @param PaidThroughPayStack $event
     * @return \Illuminate\Contracts\Auth\Authenticatable|mixed|null
     * @throws \Exception
     */
    private function getConsumer(PaidThroughPayStack $event)
    {
        if (auth()->user() instanceof Consumer) {
            return auth()->user();
        }

        $consumer = (new ConsumerRepository())->getConsumerByEmail($event->payment->response->data->customer->email);

        if ($consumer instanceof Consumer) {
            return $consumer;
        }

        throw new \Exception('Error loading consumer');
    }

    /**
     * @param $payment
     * @param bool $isDefault
     * @return mixed
     */
    private function createConsumerPaymentMethod($payment, $isDefault = false)
    {
        return PaymentMethod::create([
            'payment_provider_id' => $payment->providerId,
            'data'                => $payment->data,
            'unique_attribute'    => $payment->uniqueId,
            'last_used'           => $payment->usedAt,
            'is_default'          => $isDefault,
            'is_card'             => $payment->isCard,
            'consumer_id'         => $payment->consumerId,
        ]);
    }

    /**
     * @param $payment
     * @param Consumer $consumer
     * @return mixed
     */
    private function updateOrCreateConsumerPaymentMethod($payment, Consumer $consumer)
    {
        $result = (new PaymentMethodRepository())->findAndUpdate([
            'consumer_id'         => $payment->consumerId,
            'payment_provider_id' => $payment->providerId,
            'unique_attribute'    => $payment->uniqueId,
        ], ['last_used'           => $payment->usedAt]);

        //if(result == true) this payment method already existed and has been reused by same consumer

        if (! $result) {
            $this->createConsumerPaymentMethod($payment);
        }

        // create a function that update all payment methods of the consumer
        // by setting the most recent used method to default
        return $this->setRecentPaymentMethodToDefault($consumer);
    }

    /**
     * @param PaidThroughPayStack $event
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function getProvider(PaidThroughPayStack $event)
    {
        $provider = (new PaymentProviderRepository())->findOne(['slug' => $event->payment->provider_name]);

        if ($provider instanceof Provider) {
            return $provider;
        }

        throw new \Exception('Error loading provider');
    }

    /**
     * @param $response
     * @param Provider $provider
     * @param Consumer $consumer
     * @return \StdClass
     */
    private function getPaymentDetails($response, Provider $provider, Consumer $consumer)
    {
        $data = init();

        if ($provider->slug === PayStack_NameSlug) {
            $data->data = $response->data->authorization;
            $data->uniqueId = $response->data->authorization->signature; //unique for each card
            $data->isCard = $response->data->channel === 'card';
            $data->usedAt = $response->data->paid_at;
        }

        $data->providerId = $provider->id;
        $data->consumerId = $consumer->id;
        $data->providerSlug = $provider->slug;

        return $data;
    }

    /**
     * @param Consumer $consumer
     * @return bool
     */
    private function setRecentPaymentMethodToDefault(Consumer $consumer)
    {
        $consumerMethods = $consumer->paymentMethods();
        $recentlyUsedMethod = $consumerMethods->orderByDesc('last_used')->first();

        //reset all consumer payment method
        $consumer->paymentMethods()->each(function ($model) {
            if ($model instanceof PaymentMethod) {
                $model->updateAttributes(['is_default' => false]);
            }
        });

        if ($recentlyUsedMethod instanceof PaymentMethod) {
            //update the single record we need set to true
            return $recentlyUsedMethod->updateAttributes(['is_default' => true]);
        }

        return false;
    }
}
