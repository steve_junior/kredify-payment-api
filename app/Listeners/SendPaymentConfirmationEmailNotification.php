<?php

namespace App\Listeners;

use App\Events\PaidThroughPayStack;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPaymentConfirmationEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        //
    }
}
