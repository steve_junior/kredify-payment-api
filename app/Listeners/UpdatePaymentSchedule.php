<?php

namespace App\Listeners;

use App\Services\LogService;
use App\Services\MoneyService;
use App\Events\PaidThroughPayStack;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\PaymentScheduleRepository;
use App\Models\Payments\Schedule as PaymentSchedule;

class UpdatePaymentSchedule
{
    protected $paymentSchedules;

    /**
     * Create the event listener.
     *
     * @param PaymentScheduleRepository $repository
     */
    public function __construct(PaymentScheduleRepository $repository)
    {
        $this->paymentSchedules = $repository;
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        try {
            $response_data = $event->payment->response->data;
            $schedule = $this->paymentSchedules->findOne(['id' => $response_data->metadata->invoice]);

            if ($schedule instanceof PaymentSchedule) {
                $amount_value = MoneyService::convertAmountValue($response_data->amount, 1);
                $this->validateCurrencyAndAmount($amount_value, $response_data->currency);

                $paidAmount = MoneyService::getAmount($amount_value);
                $paidAt = $this->getTimeOfCompletion($response_data);

                $schedule->updateAttributes(['paid_amount' => $paidAmount, 'paid_at' => $paidAt]);
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_UpdatePaymentScheduleListenerError, $exception);
        }
    }

    /**
     * @param $value
     * @param $currency
     * @throws \Exception
     */
    private function validateCurrencyAndAmount($value, $currency)
    {
        if (! MoneyService::isAmountValid($value)) {
            throw new \Exception('Invalid amount value received from PayStack');
        }

        if (! MoneyService::isCurrencyValid($currency)) {
            throw new \Exception('Currency received from PayStack does not match with system currency');
        }
    }

    private static function getTimeOfCompletion($response_data)
    {
        return $response_data->paid_at;
    }
}
