<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 26/12/2020
 * Time: 11:10 AM.
 */

namespace App\Listeners;

use App\Services\LogService;
use App\Events\ConsumerAddedPhoneNumber;
use App\Repositories\ConsumerRepository;

class SendSMSVerificationNotification
{
    protected $consumers;

    /**
     * Create the event listener.
     * @param ConsumerRepository $repository
     */
    public function __construct(ConsumerRepository $repository)
    {
        $this->consumers = $repository;
    }

    /**
     * Handle the event.
     *
     * @param  ConsumerAddedPhoneNumber  $event
     * @return void
     */
    public function handle(ConsumerAddedPhoneNumber $event)
    {
        try {
            $this->consumers->getUpdatedConsumer($event->consumer,
                [
                    'sms_code_verify_phone_delivered' => true,
                    'sms_code_verify_phone_delivered_at' => now(),
                    'phone_number_verification_code' => '1111',
                ]
            );
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_SmsNotificationListenerError, $exception);
        }
    }
}
