<?php

namespace App\Listeners;

use App\Services\LogService;
use App\Services\MoneyService;
use App\Events\PaidThroughPayStack;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\PaymentEventRepository;

class UpdatePaymentEvent
{
    protected $repository;

    /**
     * Create the event listener.
     *
     * @param PaymentEventRepository $repository
     */
    public function __construct(PaymentEventRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        try {
            $response_data = $event->payment->response->data;
            $amount_value = MoneyService::convertAmountValue($response_data->amount, 1);

            if (! MoneyService::isAmountValid($amount_value)) {
                throw new \Exception('Invalid amount value received from PayStack');
            }

            if (! MoneyService::isCurrencyValid($response_data->currency)) {
                throw new \Exception('Currency received from PayStack does not match with system currency');
            }

            $this->repository->findAndUpdate(self::getReference($response_data), [
                'amount'       => MoneyService::getAmount($amount_value),
                'status'       => self::getStatus($response_data),
                'completed_at' => self::getTimeOfCompletion($response_data),
                'paystack_log' => self::getLogs($response_data),
            ]);
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_UpdatePaymentEventListenerError, $exception);
        }
    }

    private static function getTimeOfCompletion($response_data)
    {
        return $response_data->paid_at;
    }

    private static function getReference($response_data) : array
    {
        return ['reference_id' => $response_data->reference];
    }

    private static function getStatus($response_data)
    {
        if ($response_data->status === 'success') {
            $status = PAYMENT_SUCCESS;
        } elseif ($response_data->status === 'pending') {
            $status = PAYMENT_PENDING;
        } else {
            $status = PAYMENT_FAILED;
        }

        return $status;
    }

    private static function getLogs($response_data)
    {
        return $response_data->log;
    }
}
