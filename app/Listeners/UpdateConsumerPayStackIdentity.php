<?php

namespace App\Listeners;

use App\Services\LogService;
use App\Models\Consumer\Consumer;
use App\Models\Payments\Provider;
use App\Events\PaidThroughPayStack;
use App\Repositories\ConsumerRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\PaymentProviderRepository;

class UpdateConsumerPayStackIdentity
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        try {
            $provider = $this->getProvider($event);

            if ($provider->slug === PayStack_NameSlug) {
                $consumer = $this->getConsumer($event);

                if ($consumer instanceof Consumer) {
                    $consumer->updateAttributes(['paystack_identity' => $event->payment->response->data->customer]);
                }
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_UpdateConsumerPayStackIdentityListenerError, $exception);
        }
    }

    /**
     * @param PaidThroughPayStack $event
     * @return \Illuminate\Contracts\Auth\Authenticatable|mixed|null
     * @throws \Exception
     */
    private function getConsumer(PaidThroughPayStack $event)
    {
        if (auth()->user() instanceof Consumer) {
            return auth()->user();
        }

        $consumer = (new ConsumerRepository())->getConsumerByEmail($event->payment->response->data->customer->email);

        if ($consumer instanceof Consumer) {
            return $consumer;
        }

        throw new \Exception('Error loading consumer');
    }

    /**
     * @param PaidThroughPayStack $event
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    private function getProvider(PaidThroughPayStack $event)
    {
        $provider = (new PaymentProviderRepository())->findOne(['slug' => $event->payment->provider_name]);

        if ($provider instanceof Provider) {
            return $provider;
        }

        throw new \Exception('Error loading provider');
    }
}
