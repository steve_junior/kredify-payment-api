<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 05/12/2020
 * Time: 3:20 AM.
 */

namespace App\Listeners;

use App\Services\LogService;
use App\Events\PaidThroughPayStack;
use App\Repositories\CheckoutRepository;
use App\Repositories\PaymentEventRepository;
use App\Models\Payments\Event as PaymentEvent;

class UpdateCheckoutStatus
{
    protected $paymentEvents;

    protected $checkouts;

    /**
     * Create the event listener.
     *
     * @param PaymentEventRepository $paymentEventRepository
     * @param CheckoutRepository $checkoutRepository
     */
    public function __construct(PaymentEventRepository $paymentEventRepository, CheckoutRepository $checkoutRepository)
    {
        $this->paymentEvents = $paymentEventRepository;
        $this->checkouts = $checkoutRepository;
    }

    /**
     * Handle the event.
     *
     * @param  PaidThroughPayStack  $event
     * @return void
     */
    public function handle(PaidThroughPayStack $event)
    {
        try {
            $paymentEvent = $this->paymentEvents->findOne(self::getReference($event->payment->response->data));

            if ($paymentEvent instanceof PaymentEvent) {
                $this->checkouts->findAndUpdate(['id' => $paymentEvent->checkout->id], ['status' => CLIENT_APPROVED]);
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_UpdateCheckoutStatusListenerError, $exception);
        }
    }

    private static function getReference($response_data) : array
    {
        return ['reference_id' => $response_data->reference];
    }
}
