<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 9:18 AM.
 */

namespace App\Services;

use Illuminate\Support\Facades\Validator;

class RequestService
{
    public static function getValidationError(array $request, array $rules, array $messages = [])
    {
        $errors = [];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            foreach ($validator->errors()->messages() as $key => $value) {
                array_push($errors, $value);
            }
        }

        return $errors;
    }
}
