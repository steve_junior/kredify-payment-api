<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/11/2020
 * Time: 4:50 PM.
 */

namespace App\Services;

use Illuminate\Support\Str;

class GeneratorService
{
    public static function uuidString()
    {
        return str_replace('-', '', Str::uuid()->toString());
    }

    public static function numericString($type = 'int')
    {
        $reference = substr(explode(' ', microtime())[1], 0, 4).str_shuffle(explode('.', explode(' ', microtime())[0])[1]);

        if ($type == 'int') {
            return (int) $reference;
        }

        return $reference;
    }
}
