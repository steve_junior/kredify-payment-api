<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/12/2020
 * Time: 8:30 AM.
 */

namespace App\Services\Payment;

use App\Services\MoneyService;
use App\Models\Payments\Provider;
use App\Repositories\PaymentProviderRepository;

class Paystack implements Payable
{
    private $configuration;

    private $payload;

    public $provider_name;

    public $response;

    /**
     * Paystack constructor.
     * @param PaymentProviderRepository $repository
     * @throws \Exception
     */
    public function __construct(PaymentProviderRepository $repository)
    {
        $provider = $repository->getDefault();

        if ($provider instanceof Provider) {
            if ($provider->slug == PayStack_NameSlug) {
                $this->configuration = $provider->configuration;
                $this->provider_name = $provider->slug;
            } else {
                throw new \Exception('Could not load Paystack configuration');
            }
        } else {
            throw new \Exception('No default payment provider was found');
        }
    }

    /**
     * @throws \Exception
     */
    public function setProvider()
    {
    }

    /**
     * @param $payload
     * @return $this
     * @throws \Exception
     */
    public function pay($payload)
    {
        $this->payload = $payload;

        if (isset($this->configuration)) {
            $this->initialisePayStack();
        }

        return $this;
    }

    private function initialisePayStack()
    {
        $config = $this->configuration;

        $initialise_endpoint = $config->api_endpoints->initialise;

        $headers = [
            'Authorization' => 'Bearer '.env('PAYSTACK_SECRET_KEY'),
            'Content-type'  => 'application/json',
        ];

        $params = $this->getPayStackParams();

        $options = ['json' => $params, 'headers' => $headers];

        $this->response = send_http_request($initialise_endpoint, 'POST', $options);
    }

    private function getPayStackParams() : array
    {
        return [
            'amount'       => MoneyService::convertAmountValue($this->payload->amount->value),
            'currency'     => $this->configuration->currency,
            'email'        => $this->payload->email,
            'reference'    => $this->payload->reference,
            'callback_url' => route('paystack.callback'),
            'channels'     => $this->configuration->preferred_channels,
            'description'  => $this->payload->description,
            'metadata'     => $this->getMetadataField(),
        ];
    }

    private function getMetadataField()
    {
        $metadata = [];

        $metadata['custom_filters'] = ['recurring' =>  true]; //ensure paystack accepts only card with recurring payment
        $metadata['cancel_action'] = 'https://kredify.net';  //redirect consumer to URL when they cancel a payment
        $metadata['invoice'] = $this->payload->invoiceId;  //return the invoice/scheduleId with the payment response

        //return these custom fields dashboard
        $metadata['custom_fields'] = [
            json_encode([
                'display_name'  => 'InvoiceID',
                'variable_name' => 'InvoiceID',
                'value'         => $this->payload->invoiceId,
            ]),
            json_encode([
                'display_name'  => 'Description',
                'variable_name' => 'Description',
                'value'         => $this->payload->description,
            ]),
        ];

        return json_encode($metadata);
    }

    public function validate($payload)
    {
        $this->payload = $payload;

        if (isset($this->configuration)) {
            $this->verifyPayStack();
        }

        return $this;
    }

    private function verifyPayStack()
    {
        $config = $this->configuration;

        $verify_endpoint = append($config->api_endpoints->verify).$this->payload->reference;

        $options = ['headers' => ['Authorization' => 'Bearer '.env('PAYSTACK_SECRET_KEY')]];

        $this->response = send_http_request($verify_endpoint, 'GET', $options);
    }
}
