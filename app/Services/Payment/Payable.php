<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/12/2020
 * Time: 8:30 AM.
 */

namespace App\Services\Payment;

interface Payable
{
    public function setProvider();

    public function pay($payload);

    public function validate($payload);
}
