<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 04/12/2020
 * Time: 6:30 AM.
 */

namespace App\Services\Checkout\Interfaces;

interface PayableInterface
{
    public function payoutRatio() : string;

    public function payoutInterval() : int;
}
