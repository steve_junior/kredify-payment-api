<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 04/12/2020
 * Time: 5:59 AM.
 */

namespace App\Services\Checkout;

use App\Models\Checkout;
use App\Models\Consumer\Consumer;
use Illuminate\Support\Collection;
use App\Models\Consumer\RiskProfile;
use App\Repositories\RiskProfileRepository;
use App\Repositories\PaymentProviderRepository;
use App\Services\Checkout\Interfaces\PayableInterface;

/***
 * RiskAssessment decides the type of payment ratio
 * that the customer should receive based on the
 * history data & the amount of personal information provided
 * to their account.
 */
class RiskAssessmentService implements PayableInterface
{
    protected static $LOW = 1;

    protected static $MEDIUM = 2;

    protected static $HIGH = 3;

    protected static $VERY_HIGH = 4;

    /***
     * A percentage that increase based on the risk
     * assessment checks done on the consumer account
     *
     * @var int
     */
    protected $factor = 0;

    protected $score = 0;

    protected $assessmentList;

    protected $consumer;

    protected $checkout;

    protected $riskLevel;

    protected $riskProfileRepository;

    /**
     * RiskAssessmentService constructor.
     * @param Checkout $checkout
     * @throws \Exception
     */
    public function __construct(Checkout $checkout)
    {
        $consumer = auth()->user();

        if (isset($consumer) && $consumer instanceof Consumer) {
            $consumer->load(['paymentMethods', 'paymentSchedules', 'paymentEvents']);

            $this->consumer = $consumer;
            $this->checkout = $checkout->load(['relatedOrder']);
            $this->assessmentList = init();
            $this->riskProfileRepository = new RiskProfileRepository();
        } else {
            throw new \Exception('No consumer available');
        }
    }

    private function updateRiskFactor()
    {
        $current_factor = $this->factor;
        $new_factor = $current_factor + $this->score;
        $this->factor = $new_factor;
    }

    private function updateAssessmentScore()
    {
        //set points to 5 since the sum of the assessment is 95;
        $score = 5;

        if (! $this->assessmentList->has_consumer_made_complete_payment_prior) {
            $score += 25;
        }

        // is the consumer information correlate with the order.consumer info?
        if (! $this->assessmentList->is_consumer_info_matching_with_order_info) {
            $score += 25;
        }

        // is the most payment information reusable?
        if (! $this->assessmentList->is_recent_payment_method_reusable) {
            $score += 25;
        }

        if (! $this->assessmentList->is_consumer_email_verified) {
            $score += 10;
        }

        if (! $this->assessmentList->is_consumer_phone_number_verified) {
            $score += 10;
        }

        $this->score = $score;
    }

    /**
     *  Adjust Risk Level calculation.
     */
    private function updateRiskLevel()
    {
        if ($this->factor <= 25) {
            $this->riskLevel = self::$LOW;
        }

        if ($this->factor > 25 && $this->factor <= 50) {
            $this->riskLevel = self::$MEDIUM;
        }

        if ($this->factor > 50 && $this->factor <= 75) {
            $this->riskLevel = self::$HIGH;
        }

        if ($this->factor > 75) {
            $this->riskLevel = self::$VERY_HIGH;
        }
    }

    /**
     * Run a list of checks to assess the consumer
     * risk factor.
     */
    public function runAssessment()
    {
        $this->assessmentList->has_consumer_made_complete_payment_prior = $this->hasConsumerMadeCompletePaymentPrior();
        $this->assessmentList->is_consumer_info_matching_with_order_info = $this->isConsumerPersonalInfoCorrelatingWithOrderInfo();
        $this->assessmentList->is_recent_payment_method_reusable = $this->isMostRecentPaymentMethodReusable();
        $this->assessmentList->is_consumer_email_verified = $this->isConsumerEmailVerified();
        $this->assessmentList->is_consumer_phone_number_verified = $this->isConsumerPhoneNumberVerified();

        $this->updateAssessmentScore();
        $this->updateRiskFactor();
        $this->updateRiskLevel();

        return $this;
    }

    public function getRiskProfile()
    {
        $model = [];

        $model['level'] = $this->riskLevel;
        $model['allowed_payout_ratio'] = $this->payoutRatio();
        $model['allowed_intervals_in_days'] = $this->payoutInterval();
        $model['factor'] = $this->factor;
        $model['assessment_score'] = $this->score;
        $model['consumer_id'] = auth()->id();

        $profile = $this->riskProfileRepository->findOne($model);

        if (isset($profile)) {
            return $profile;
        }

        $model['assessment'] = $this->assessmentList;

        return RiskProfile::create($model);
    }

    public function payoutRatio(): string
    {
        switch ($this->riskLevel) {
            case self::$LOW:
                return config(CONFIG_SETTINGS_RATIO_OF_PAYOUTS_LOW_RISK);

            case self::$MEDIUM:
                return config(CONFIG_SETTINGS_RATIO_OF_PAYOUTS_NORMAL_RISK);

            case self::$HIGH:
                return config(CONFIG_SETTINGS_RATIO_OF_PAYOUTS_HIGH_RISK);

            case self::$VERY_HIGH:
            default:
                return config(CONFIG_SETTINGS_RATIO_OF_PAYOUTS_VERY_HIGH_RISK);
        }
    }

    public function payoutInterval() : int
    {
        return config(CONFIG_SETTINGS_NUMBER_OF_DAYS_PER_PAYOUTS);
    }

    // List Risk assessment functions
    protected function isConsumerPhoneNumberVerified()
    {
        return $this->consumer->is_phone_number_verified;
    }

    protected function isConsumerEmailVerified() : bool
    {
        return $this->consumer->is_email_verified;
    }

    protected function isMostRecentPaymentMethodReusable() : bool
    {
        $paymentMethods = $this->consumer->paymentMethods->sortByDesc('last_used');

        if (! ($paymentMethods instanceof Collection)) {
            return false;
        }

        if ($paymentMethods->isEmpty()) {
            return false;
        }

        $recent_payment_method = $paymentMethods->first();

        $provider = (new PaymentProviderRepository())->findOne(['id' => $recent_payment_method->payment_provider_id]);

        if (! isset($provider)) {
            return false;
        }

        if ($provider->slug === PayStack_NameSlug) {
            //is payStack
            return $recent_payment_method->data->reusable;
        }

        return false;
    }

    protected function isConsumerPersonalInfoCorrelatingWithOrderInfo()
    {
        $consumer = auth()->user();
        $checkedOutOrder = $this->checkout->relatedOrder;
        $consumerInfoOnOrder = $checkedOutOrder->consumer();
        $orderRecipientInfo = $checkedOutOrder->orderRecipient();

        if (isset($consumerInfoOnOrder)) {
            $orderConsumer = $consumerInfoOnOrder;
        } elseif (isset($orderRecipientInfo)) {
            $orderConsumer = $orderRecipientInfo;
        } else {
            return false;
        }

        return string_matches($consumer->full_name, $orderConsumer->name)
            && string_matches($consumer->email, $orderConsumer->email);
    }

    protected function hasConsumerMadeCompletePaymentPrior()
    {
        $approvedCheckout = $this->consumer->latestApprovedCheckout;

        if (! isset($approvedCheckout)) {
            return false;
        }

        $successPaymentEvents = $this->consumer->successfulPaymentEventsByCheckout($approvedCheckout->id);

        if ($successPaymentEvents->isEmpty()) {
            return false;
        }

        if ($approvedCheckout instanceof Checkout) {
            $totalAmountRequested = optional($approvedCheckout->relatedOrder)->totalAmount();
            $totalAmountValue = floatval($totalAmountRequested->value);

            $amountPaid = $successPaymentEvents->sum('amount.value');

            return float_matches($amountPaid, $totalAmountValue);
        }

        return false;
    }
}
