<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/11/2020
 * Time: 6:16 AM.
 */

namespace App\Services\Checkout;

use App\Models\Checkout;
use App\Services\LogService;
use App\Services\GeneratorService;
use Illuminate\Support\Collection;
use App\Transformers\JsonStructure;
use App\Repositories\CheckoutRepository;
use App\Models\Payments\Event as PaymentEvent;
use App\Repositories\PaymentScheduleRepository;
use App\Models\Payments\Schedule as PaymentSchedule;

class CheckoutService
{
    protected $checkouts;

    protected $schedules;

    protected $riskProfile;

    public function __construct(CheckoutRepository $checkoutRepository, PaymentScheduleRepository $scheduleRepository)
    {
        $this->checkouts = $checkoutRepository;
        $this->schedules = $scheduleRepository;
    }

    /**
     * Get the checkout model using the checkoutToken
     * & loads relationships.
     *
     * @param $token
     * @return Checkout
     */
    public function getCheckoutByToken($token)
    {
        if (is_null($token)) {
            $token = session(Session_CurrentCheckoutToken);
        }

        if (is_null($token)) {
            abort(404);
        }

        $checkout = $this->checkouts->findOne(['token' => $token]);

        if ($checkout instanceof Checkout) {
            $checkout->load(['merchantAccount', 'relatedOrder', 'consumer']);
        }

        return $checkout;
    }

    /**
     *  Binds the checkout to the logged in consumer.
     *
     * @param Checkout $checkout
     * @return bool status
     */
    public function bindCheckoutToAuth(Checkout $checkout)
    {
        if (! $checkout->hasOwner()) {
            return $checkout->updateAttributes(['consumer_id' => auth()->id()]);
        }

        return false;
    }

    /**
     * @param Checkout $checkout
     * @return \Exception|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Http\JsonResponse
     */
    public function getOrCreateSchedules(Checkout $checkout)
    {
        $schedules = $this->schedules->find(['checkout_id' => $checkout->id]);

        if ($schedules instanceof Collection && $schedules->isNotEmpty()) {
            return $schedules;
        }

        try {
            $riskAssessment = new RiskAssessmentService($checkout);
            $this->riskProfile = $riskAssessment->runAssessment()->getRiskProfile();

            $total_amount = optional($checkout->relatedOrder)->totalAmount();
            $paymentPlan = $this->getPaymentPlan($total_amount);

            foreach ($paymentPlan as $due_date => $debt) {
                PaymentSchedule::create([
                   'checkout_id'      => $checkout->id,
                   'original_amount'  => $debt,
                   'due_date'         => $due_date,
               ]);
            }
        } catch (\Exception $exception) {
            LogService::CriticalLog(Log_GetCheckoutSchedulesError, $exception, auth()->user());

            abort(500);
        }

        return $this->schedules->find(['checkout_id' => $checkout->id]);
    }

    /**
     * @param $money
     * @return  array
     */
    private function getDueDebtAmounts($money) : array
    {
        $payoutRatio = $this->riskProfile->allowed_payout_ratio;
        $amounts = [];

        foreach ($payoutRatio as $ratio) {
            $ratio = hundredth($ratio);

            $amount = init();
            $amount->value = round(floatval($money->value) * $ratio, 2);
            $amount->currency = config(CONFIG_SETTINGS_CURRENCY);

            array_push($amounts, $amount);
        }

        return $amounts;
    }

    private function getPaymentPlan($money) : array
    {
        $debtAmountList = $this->getDueDebtAmounts($money);
        $dueDateList = $this->getDueDates();

        return array_combine($dueDateList, $debtAmountList);
    }

    private function getDueDates() : array
    {
        $payout_days = $this->riskProfile->allowed_intervals_in_days;
        $dates = [];

        for ($i = 0; $i <= config(CONFIG_SETTINGS_NUMBER_OF_PAYOUTS) - 1; $i++) {
            if ($i == 0) {
                $dates[$i] = now();

                continue;
            }
            $dates[$i] = now()->addDays($payout_days);
            $payout_days += $this->riskProfile->allowed_intervals_in_days;
        }

        return $dates;
    }

    /**
     * @param $schedule_id
     * @return bool|\StdClass
     * @throws \Exception
     */
    public function createInvoiceFromSchedule($schedule_id)
    {
        $schedule = $this->schedules->findOne(['id' => $schedule_id]);

        if ($schedule instanceof PaymentSchedule) {
            if ($schedule->paidInFull()) {
                return false;
            }

            //create a payment event
            $event = PaymentEvent::create([
                'checkout_id'  => $schedule->checkout_id,
                'reference_id' => GeneratorService::numericString(),
                'amount'       => $schedule->original_amount,
            ]);

            $invoice = init();
            $invoice->amount = $event->amount;
            $invoice->email = auth()->user()->email;
            $invoice->invoiceId = $schedule_id;
            $invoice->reference = $event->reference_id;
            $invoice->description = $invoice->email.' pay '.$invoice->amount->currency.' '.$invoice->amount->value.' for invoice '.$invoice->invoiceId.' using reference '.$invoice->reference;

            return $invoice;
        }

        return false;
    }
}
