<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/11/2020
 * Time: 7:08 PM.
 */

namespace App\Services;

use Exception;
use App\Models\Consumer\Consumer;
use Illuminate\Support\Facades\Log;

class LogService
{
    protected static function format($logType, Exception $exception, $email)
    {
        $message = $exception->getMessage().' >> ';
        $message .= $exception->getFile().' >> ';
        $message .= 'line '.$exception->getLine();

        if (! is_empty($email)) {
            $message .= ' >>  user '.$email;
        }

        return $logType.': '.$message;
    }

    public static function ErrorLog($logType, Exception $exception, $context = null)
    {
        $email = is_null($context) ? '' : self::getContext($context);

        Log::error(self::format($logType, $exception, $email));
    }

    public static function InfoLog($logType, Exception $exception, $context = null)
    {
        $email = is_null($context) ? '' : self::getContext($context);

        Log::info(self::format($logType, $exception, $email));
    }

    public static function CriticalLog($logType, Exception $exception, $context = null)
    {
        $email = is_null($context) ? '' : self::getContext($context);

        Log::critical(self::format($logType, $exception, $email));
    }

    public static function getContext($context)
    {
        if ($context instanceof Consumer) {
            return $context->email;
        }

        if (auth()->user() instanceof Consumer) {
            return auth()->user();
        }
    }
}
