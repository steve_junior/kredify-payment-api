<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 02/12/2020
 * Time: 5:14 AM.
 */

namespace App\Services;

use Exception;
use Cknow\Money\Money;

class MoneyService
{
    /*
     * Parses the raw amount like 2.50 to standard system amount object
     * return Money | null
     */
    private static function parse($value)
    {
        $money = null;

        try {
            if (! is_numeric($value)) {
                throw new Exception('Parsed amount can only numeric');
            }

            if (floatval($value) < 0) {
                throw new Exception('Parsed amount cannot be negative');
            }

            $amount_in_string = number_format(floatval($value), 2, '.', '');
            $money = Money::parseByDecimal($amount_in_string, config(CONFIG_SETTINGS_CURRENCY));
        } catch (Exception $exception) {
            LogService::ErrorLog(Log_MoneyParseError, $exception);

            return;
        }

        return $money;
    }

    public static function isAmountValid($value)
    {
        return isNotNull(self::parse($value));
    }

    public static function isCurrencyValid($currency)
    {
        return $currency === config(CONFIG_SETTINGS_CURRENCY);
    }

    /***
     * Convert any monetary value to system amount -> {value, currency}
     *
     * @param $value
     * @return \StdClass
     */
    public static function getAmount($value)
    {
        $money = self::parse($value);

        $data = init();

        $data->value = hundredth($money->getAmount());
        $data->currency = $money->getCurrency()->getCode();

        return $data;
    }

    /**
     * Convert monetary value into any denomination and return the new value.
     *
     * @param $value
     * @param int $denomination -> 0 = Lowest, 1 = Highest
     * @return float|int|string
     */
    public static function convertAmountValue($value, $denomination = 0)
    {
        $formatted_value = number_format((float) $value + 0, 2, '.', '');

        if ($denomination) {
            return hundredth($formatted_value);
        }

        return intval(round($formatted_value * 100));
    }
}
