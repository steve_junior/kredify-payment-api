<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/11/2020
 * Time: 6:50 AM.
 */

namespace App\Services;

use App\Models\Checkout;
use App\Models\Consumer\Consumer;
use Illuminate\Support\Collection;
use App\Models\Payments\Schedule as PaymentSchedule;

class ResponseService
{
    /**
     * Create a temp object that is used in the frontend or api response.
     *
     * @param Checkout $checkout
     * @param $schedules
     * @return \StdClass
     */
    public function createCheckout(Checkout $checkout, $schedules)
    {
        $data = init();

        $data->storeName = optional($checkout->merchantAccount)->name;
        $data->consumer = optional($checkout->relatedOrder)->consumer();
        $data->totalAmount = optional($checkout->relatedOrder)->totalAmount();
        $data->orderNumber = optional($checkout->relatedOrder)->orderNumber();

        $paymentPlans = collect();

        if ($schedules instanceof Collection) {
            $schedules->sortBy('due_date')->each(function ($item) use ($paymentPlans) {
                $schedule = init();

                if ($item instanceof PaymentSchedule) {
                    $schedule->invoiceId = $item->id;
                    $schedule->isPaid = $item->paidInFull();
                    $schedule->dueDate = $item->due_date->toDateTimeString();
                    $schedule->amount = $item->original_amount;

                    if ($schedule->isPaid) {
                        $schedule->paidAmount = $item->paid_amount;
                        $schedule->paidAt = $item->paid_at;
                    } else {
                        $schedule->isOverDue = $item->isOverDue();
                    }
                }

                return $paymentPlans->add($schedule);
            });
        }

        $data->plan = $paymentPlans;

        return $data;
    }

    public function regPhoneNumber(Consumer $consumer, object $checkList)
    {
        $data = init();

        $data->hasPhoneNumber = $checkList->hasPhoneNumber;
        $data->hasPhoneNumberVerified = $checkList->isPhoneNumberVerified;
        $data->smsSent = $checkList->isSmsDelivered;

        $data->countryCode = config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2);
        $data->regId = $consumer->registration_id;

        return $data;
    }

    public function initialisedScan(Consumer $consumer, object $checkList)
    {
        // careful exposing any private info about the consumer since the consumer is not fully logged in yet.
        $data = init();

        $data->isNew = ! $checkList->allChecksPassed;
        $data->hasPassword = $checkList->hasPassword;
        $data->hasPhoneNumber = $checkList->hasPhoneNumber;
        $data->hasPhoneNumberVerified = $checkList->isPhoneNumberVerified;
        $data->hasLegalName = $checkList->hasLegalName;
        $data->hasAddress = $checkList->hasAddress;

        $data->defaultLang = config('app.locale');
        $data->defaultCurrency = config(CONFIG_SETTINGS_CURRENCY);

        //if all checks passes, respond with the appropriate body allowing the client to ask for password
        if (! $checkList->allChecksPassed) {
            $data->regId = $consumer->registration_id;
        }

        if ($checkList->hasPhoneNumber && ! $checkList->isPhoneNumberVerified) {
            $data->phoneNumber = $consumer->phone_number;
            $data->countryCode = config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2);
        }

        return $data;
    }

    public function payStackAuthorisation(object $response)
    {
        $data = init();

        $data->paymentURL = $response->data->authorization_url;

        return $data;
    }
}
