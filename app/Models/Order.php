<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 21/07/2020
 * Time: 2:51 PM.
 */

namespace App\Models;

use App\Models\Commons\CommonBase;

class Order extends CommonBase
{
    public function getConsumerAttribute($value)
    {
        return jsonDecode($value);
    }

    public function getOrderRecipientAttribute($value)
    {
        return jsonDecode($value);
    }

    public function getTotalAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function consumer()
    {
        return $this->consumer;
    }

    public function totalAmount()
    {
        return $this->total_amount;
    }

    public function orderRecipient()
    {
        return $this->order_recipient;
    }

    public function orderNumber()
    {
        if (isset($this->order_number)) {
            return $this->order_number;
        }

        return (string) $this->system_generated_order_number;
    }
}
