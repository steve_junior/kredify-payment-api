<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/12/2020
 * Time: 3:56 PM.
 */

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;
use App\Models\Commons\Interfaces\UpdatableInterface;

class Base extends Model implements UpdatableInterface
{
    /**
     *  Update the model instance with new attributes.
     *
     * @param  array $attributes
     *
     * @return bool
     */
    public function updateAttributes(array $attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
            $this->{$key} = $attribute;
        }

        return $this->save();
    }
}
