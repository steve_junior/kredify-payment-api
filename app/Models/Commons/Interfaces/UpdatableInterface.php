<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/12/2020
 * Time: 3:42 PM.
 */

namespace App\Models\Commons\Interfaces;

interface UpdatableInterface
{
    /**
     *  Update the model instance with new attributes.
     *
     * @param  array $attributes
     *
     * @return bool
     */
    public function updateAttributes(array $attributes);
}
