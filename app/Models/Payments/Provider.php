<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/12/2020
 * Time: 3:50 AM.
 */

namespace App\Models\Payments;

use Illuminate\Support\Str;
use App\Services\GeneratorService;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'payment_providers';

    public $incrementing = false;

    protected $fillable = ['name', 'slug'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($provider) {
            $provider->slug = Str::slug($provider->name);
            $provider->{$provider->getKeyName()} = GeneratorService::uuidString();
        });
    }

    public function setConfigurationAttribute($value)
    {
        $this->attributes['configuration'] = jsonEncode($value);
    }

    public function getConfigurationAttribute($value)
    {
        return jsonDecode($value);
    }
}
