<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 29/11/2020
 * Time: 8:54 PM.
 */

namespace App\Models\Payments;

use App\Models\Checkout;
use App\Models\Commons\CommonBase;
use App\Services\GeneratorService;

class Event extends CommonBase
{
    protected $table = 'payment_events';

    protected $fillable = [
        'checkout_id', 'reference_id', 'amount',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'attempted_at' => 'datetime',
        'completed_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($event) {
            $event->{$event->getKeyName()} = GeneratorService::uuidString();
            $event->attempted_at = now();
        });
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = jsonEncode($value);
    }

    public function getAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setPaystackLogAttribute($value)
    {
        $this->attributes['paystack_log'] = jsonEncode($value);
    }

    public function getPaystackLogAttribute($value)
    {
        return jsonDecode($value);
    }

    public function checkout()
    {
        return $this->belongsTo(Checkout::class);
    }
}
