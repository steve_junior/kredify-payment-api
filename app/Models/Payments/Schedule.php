<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/11/2020
 * Time: 12:12 PM.
 */

namespace App\Models\Payments;

use Carbon\Carbon;
use App\Models\Commons\CommonBase;
use App\Services\GeneratorService;

class Schedule extends CommonBase
{
    protected $table = 'payment_schedules';

    protected $keyType = 'string';

    protected $fillable = [
        'checkout_id', 'original_amount', 'due_date',
    ];

    public $incrementing = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'due_date' => 'datetime',
        'paid_at'  => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($schedule) {
            $schedule->consumer_id = auth()->id();
            $schedule->{$schedule->getKeyName()} = GeneratorService::uuidString();
        });
    }

    public function setOriginalAmountAttribute($value)
    {
        $this->attributes['original_amount'] = jsonEncode($value);
    }

    public function getOriginalAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function setPaidAmountAttribute($value)
    {
        $this->attributes['paid_amount'] = jsonEncode($value);
    }

    public function getPaidAmountAttribute($value)
    {
        return jsonDecode($value);
    }

    public function isOverDue()
    {
        return Carbon::parse($this->due_date)->isPast();
    }

    public function paidInFull()
    {
        $owedAmount = optional($this->original_amount)->value;
        $paidAmount = optional($this->paid_amount)->value;

        if (is_null($owedAmount) || is_null($paidAmount)) {
            return false;
        }

        return $owedAmount == $paidAmount;
    }

    public function paidInPart()
    {
        return ! $this->paidInFull();
    }
}
