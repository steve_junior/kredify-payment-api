<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 01/12/2020
 * Time: 3:49 AM.
 */

namespace App\Models\Payments;

use App\Models\Consumer\Consumer;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payments\Provider as PaymentProvider;

class Method extends Model
{
    protected $table = 'payment_methods';

    protected $fillable = ['payment_provider_id', 'data', 'unique_attribute', 'last_used', 'is_default', 'is_card', 'consumer_id'];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = jsonEncode($value);
    }

    public function getDataAttribute($value)
    {
        return jsonDecode($value);
    }

    /**
     *  Update the model instance with new attributes.
     *
     * @param  array $attributes
     *
     * @return bool
     */
    public function updateAttributes(array $attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
            $this->{$key} = $attribute;
        }

        return $this->save();
    }

    public function owner()
    {
        return $this->belongsTo(Consumer::class, 'consumer_id');
    }

    public function provider()
    {
        return $this->belongsTo(PaymentProvider::class, 'payment_provider_id');
    }
}
