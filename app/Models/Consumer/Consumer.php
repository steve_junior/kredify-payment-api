<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/11/2020
 * Time: 7:16 PM.
 */

namespace App\Models\Consumer;

use App\Models\Checkout;
use App\Models\Commons\AuthBase;
use App\Services\GeneratorService;
use Illuminate\Notifications\Notifiable;
use App\Models\Payments\Event as PaymentEvent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Payments\Method as PaymentMethod;
use App\Models\Payments\Schedule as PaymentSchedule;
use Illuminate\Foundation\Auth\User as Authenticatable;

/*
 * For the consumer to use the email verification enabled by fortify
 * Consumer class must implement MustVerifyEmail interface
 */
class Consumer extends AuthBase
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'registration_id', 'paystack_identity', 'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
        'date_of_birth' => 'datetime',
        'recent_login' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($consumer) {
            $consumer->registration_id = GeneratorService::uuidString();
        });
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function setPaystackIdentityAttribute($value)
    {
        $this->attributes['paystack_identity'] = jsonEncode($value);
    }

    public function getPaystackIdentityAttribute($value)
    {
        return jsonDecode($value);
    }

    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class, 'consumer_id');
    }

    public function paymentSchedules()
    {
        return $this->hasMany(PaymentSchedule::class, 'consumer_id');
    }

    public function successfulPaymentEvents()
    {
        return $this->hasManyThrough(PaymentEvent::class, Checkout::class)->where('status', PAYMENT_SUCCESS);
    }

    public function successfulPaymentEventsByCheckout($checkoutId)
    {
        return $this->hasManyThrough(PaymentEvent::class, Checkout::class)->get()->where('status', PAYMENT_SUCCESS)->where('checkout_id', $checkoutId);
    }

    public function paymentEvents()
    {
        return $this->hasManyThrough(PaymentEvent::class, Checkout::class);
    }

    public function latestApprovedCheckout()
    {
        return $this->hasOne(Checkout::class)->where('status', CLIENT_APPROVED)->latest();
    }

    public function checkouts()
    {
        return $this->hasMany(Checkout::class, 'consumer_id');
    }
}
