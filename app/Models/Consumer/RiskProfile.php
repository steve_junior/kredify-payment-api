<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 05/12/2020
 * Time: 12:51 AM.
 */

namespace App\Models\Consumer;

use App\Models\Commons\CommonBase;

class RiskProfile extends CommonBase
{
    protected $table = 'risk_profiles';

    protected $fillable = [
        'level',
        'factor',
        'assessment_score',
        'allowed_intervals_in_days',
        'allowed_payout_ratio',
        'assessment',
    ];

    protected $casts = [
        'assessed_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($risk_profile) {
            $risk_profile->assessed_at = now();
            $risk_profile->consumer_id = auth()->id();
        });
    }

    public function setAssessmentAttribute($value)
    {
        $this->attributes['assessment'] = jsonEncode($value);
    }

    public function getAssessmentAttribute($value)
    {
        return jsonDecode($value);
    }

    public function getAllowedPayoutRatioAttribute($value)
    {
        return explode(':', $value);
    }
}
