<?php

namespace App\Models;

use App\Models\Consumer\Consumer;
use App\Models\Commons\CommonBase;
use App\Models\Payments\Event as PaymentEvent;

class Checkout extends CommonBase
{
    protected $table = 'checkouts';

    protected $hidden = [
        'full_link', 'merchant_account_id',
    ];

    protected $dates = [
        'expired_at',
    ];

    public function getStatusAttribute($value)
    {
        return customerCheckoutStatus($value);
    }

    public function hasExpired()
    {
        return $this->expired_at->isPast();
    }

    public function hasOwner()
    {
        return ! is_null($this->consumer);
    }

    public function belongsToLoggedInConsumer()
    {
        if (! $this->hasOwner()) {
            return false;
        }

        return $this->consumer->id == auth()->id();
    }

    public function relatedOrder()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function merchantAccount()
    {
        return $this->belongsTo(MerchantAccount::class, 'merchant_account_id');
    }

    public function consumer()
    {
        return $this->belongsTo(Consumer::class, 'consumer_id');
    }

    public function paymentEvents()
    {
        return $this->hasMany(PaymentEvent::class);
    }
}
