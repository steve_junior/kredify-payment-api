<?php

namespace App\Providers;

use App\Events\PaidThroughPayStack;
use App\Listeners\UpdatePaymentEvent;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\UpdateCheckoutStatus;
use App\Events\ConsumerAddedPhoneNumber;
use App\Listeners\UpdatePaymentSchedule;
use App\Listeners\UpdateConsumerPaymentMethod;
use App\Listeners\UpdateConsumerPayStackIdentity;
use App\Listeners\SendSMSVerificationNotification;
use App\Listeners\SendPaymentConfirmationEmailNotification;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ConsumerAddedPhoneNumber::class => [
            SendSMSVerificationNotification::class,
        ],
        PaidThroughPayStack::class => [
            UpdatePaymentEvent::class,
            UpdatePaymentSchedule::class,
            UpdateConsumerPaymentMethod::class,
            UpdateConsumerPayStackIdentity::class,
            SendPaymentConfirmationEmailNotification::class,
            UpdateCheckoutStatus::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
