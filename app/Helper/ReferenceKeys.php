<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/11/2020
 * Time: 5:46 AM.
 */
const CONFIG_SETTINGS = 'app_settings.';
const CONFIG_SETTINGS_CURRENCY = CONFIG_SETTINGS.'currency';
const CONFIG_SETTINGS_COUNTRY = CONFIG_SETTINGS.'country';
const CONFIG_SETTINGS_COUNTRYCODE_ALPHA3 = CONFIG_SETTINGS.'countryCodeAlpha3';
const CONFIG_SETTINGS_COUNTRYCODE_ALPHA2 = CONFIG_SETTINGS.'countryCodeAlpha2';
const CONFIG_SETTINGS_WHITELISTED_IPS = CONFIG_SETTINGS.'whitelistedIps';

const CONFIG_SETTINGS_NUMBER_OF_PAYOUTS = CONFIG_SETTINGS.'numberOfPayouts';
const CONFIG_SETTINGS_NUMBER_OF_DAYS_PER_PAYOUTS = CONFIG_SETTINGS.'numberOfDaysPerPayouts';
const CONFIG_SETTINGS_RATIO_OF_PAYOUTS_LOW_RISK = CONFIG_SETTINGS.'ratioOfPayoutsLowRisk';
const CONFIG_SETTINGS_RATIO_OF_PAYOUTS_NORMAL_RISK = CONFIG_SETTINGS.'ratioOfPayoutsNormalRisk';
const CONFIG_SETTINGS_RATIO_OF_PAYOUTS_HIGH_RISK = CONFIG_SETTINGS.'ratioOfPayoutsHighRisk';
const CONFIG_SETTINGS_RATIO_OF_PAYOUTS_VERY_HIGH_RISK = CONFIG_SETTINGS.'ratioOfPayoutsVeryHighRisk';

const CONFIG_APP = 'app.';
const CONFIG_APP_LOCALE_LANG = CONFIG_APP.'locale';

//Error Log Types
const Log_DatabaseConnectionError = 'DatabaseConnectionError';
const Log_PaymentProviderError = 'PaymentProviderError';
const Log_TransactionNotFound = 'TransactionNotFound';
const Log_PaystackTransactionVerificationError = 'PaystackTransactionVerificationError ';
const Log_MoneyParseError = 'MoneyParseError';
const Log_UpdatePaymentEventListenerError = 'UpdatePaymentEventListenerError';
const Log_UpdatePaymentScheduleListenerError = 'UpdatePaymentScheduleListenerError';
const Log_UpdateCheckoutStatusListenerError = 'UpdateCheckoutStatusListenerError';
const Log_SmsNotificationListenerError = 'SmsNotificationListenerError';
const Log_UpdateConsumerPaymentMethodListenerError = 'UpdateConsumerPaymentMethodListenerError';
const Log_UpdateConsumerPayStackIdentityListenerError = 'UpdateConsumerPayStackIdentityListenerError';
const Log_AppExceptionHandler = 'AppExceptionHandler';
const Log_GetCheckoutSchedulesError = 'GetCheckoutSchedulesError';

//Session Keys
const Session_CurrentCheckoutToken = 'current_checkout_token';

const PayStack_NameSlug = 'paystack-limited';
