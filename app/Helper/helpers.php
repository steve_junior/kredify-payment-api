<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 21/07/2020
 * Time: 6:08 AM.
 */

//Customer Checkout Status
if (! function_exists('customerCheckoutStatus')) {
    function customerCheckoutStatus($key = null)
    {
        $data = [
            CLIENT_APPROVED => __('Approved'),
            CLIENT_DECLINED => __('Declined'),
            CLIENT_DORMANT  => __('Dormant'),
        ];

        if ($key == null) {
            return $data;
        } else {
            return $data[$key];
        }
    }
}

//Get Http Messages
if (! function_exists('getHttpMessages')) {
    function getHttpMessages($key = null)
    {
        $data = [
            RESPONSE_SUCCESS,
            RESPONSE_FAILED,
            RESPONSE_BAD_REQUEST,
            RESPONSE_SERVICE_UNAVAILABLE,
            RESPONSE_UNAUTHORISED,
            RESPONSE_UNAUTHENTICATED,
        ];

        if ($key == null) {
            return $data;
        } else {
            return $data[$key];
        }
    }
}

//Get Http Codes
if (! function_exists('getHttpCodes')) {
    function getHttpCodes()
    {
        return array_merge(getSuccessHttpCodes(), getClientErrorHttpCodes(), getServerErrorHttpCodes());
    }
}

if (! function_exists('getSuccessHttpCodes')) {
    function getSuccessHttpCodes()
    {
        return [
            200, 201, 204,
        ];
    }
}

if (! function_exists('getClientErrorHttpCodes')) {
    function getClientErrorHttpCodes()
    {
        return [
            400, 401, 403, 404, 405, 422,
        ];
    }
}

if (! function_exists('getServerErrorHttpCodes')) {
    function getServerErrorHttpCodes()
    {
        return [
            500, 501, 503,
        ];
    }
}
