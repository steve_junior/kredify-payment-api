<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/11/2020
 * Time: 1:13 PM.
 */
if (! function_exists('jsonEncode')) {
    function jsonEncode($value)
    {
        return isset($value) ? json_encode($value) : null;
    }
}

if (! function_exists('jsonDecode')) {
    function jsonDecode($value, $assoc = false)
    {
        return isset($value) ? json_decode($value, $assoc) : null;
    }
}
