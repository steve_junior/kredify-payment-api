<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/11/2020
 * Time: 5:58 AM.
 */

use GuzzleHttp\Client;

if (! function_exists('init')) {
    function init()
    {
        return new StdClass;
    }
}

if (! function_exists('append')) {
    function append($str, $char = '/')
    {
        return rtrim($str, $char).$char;
    }
}

if (! function_exists('isNotNull')) {
    function isNotNull($value)
    {
        return ! is_null($value);
    }
}

if (! function_exists('hundredth')) {
    function hundredth($value)
    {
        return floatval($value) / 100;
    }
}

if (! function_exists('string_matches')) {
    function string_matches($name1, $name2)
    {
        return preg_match("/$name1/ui", $name2);
    }
}

if (! function_exists('float_matches')) {
    function float_matches($operand1, $operand2)
    {
//        return abs(floatval($operand1) - floatval($operand2)) < 1;
        return round($operand1, 2) == round($operand2, 2);
    }
}

if (! function_exists('send_http_request')) {
    function send_http_request($url, $method = 'GET', $options = [], $return = true)
    {
        $client = new Client();
        $response = null;
        $method = strtolower($method);

        if ($method == 'get') {
            $response = $client->get($url, $options);
        }

        if ($method == 'post') {
            $response = $client->post($url, $options);
        }

        if ($return) {
            return json_decode($response->getBody()->getContents());
        }
    }
}

if (! function_exists('is_empty')) {
    /**
     * @param $value
     * @return bool
     */
    function is_empty($value)
    {
        if (is_string($value)) {
            return strlen($value) === 0;
        }

        if (is_array($value)) {
            return count($value) === 0;
        }

        return false;
    }
}

if (! function_exists('is_true')) {
    /**
     * @param $value
     * @return bool
     */
    function is_true($value)
    {
        if (is_bool($value)) {
            return $value === true;
        }

        if (is_numeric($value)) {
            return (bool) $value;
        }

        return false;
    }
}

if (! function_exists('is_whitelisted')) {
    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    function is_whitelisted(Illuminate\Http\Request $request)
    {
        return in_array($request->ip(), config(CONFIG_SETTINGS_WHITELISTED_IPS));
    }
}
