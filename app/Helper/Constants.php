<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 27/07/2020
 * Time: 4:38 AM.
 */

//Client Payment Statuses
const CLIENT_APPROVED = 800;
const CLIENT_DECLINED = 801;
const CLIENT_DORMANT = 802;

const PAYMENT_SUCCESS = 'success';
const PAYMENT_FAILED = 'failed';
const PAYMENT_PENDING = 'pending';
const PAYMENT_INITIATED = 'initiated';
const PAYMENT_STATUSES = [PAYMENT_SUCCESS, PAYMENT_FAILED, PAYMENT_PENDING, PAYMENT_INITIATED];

//http response messages
const RESPONSE_SUCCESS = 'success';
const RESPONSE_FAILED = 'failed';
const RESPONSE_BAD_REQUEST = 'bad_request';
const RESPONSE_SERVICE_UNAVAILABLE = 'service_unavailable';
const RESPONSE_OUTBOUND_SERVICE_UNAVAILABLE = 'external_service_unavailable';
const RESPONSE_UNAUTHORISED = 'unauthorised';
const RESPONSE_UNAUTHENTICATED = 'unauthenticated';
