<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 26/12/2020
 * Time: 11:09 AM.
 */

namespace App\Events;

use App\Models\Consumer\Consumer;
use Illuminate\Queue\SerializesModels;

class ConsumerAddedPhoneNumber
{
    use SerializesModels;

    /**
     * The authenticated user.
     */
    public $consumer;

    /**
     * Create a new event instance.
     *
     * @param Consumer $consumer
     */
    public function __construct(Consumer $consumer)
    {
        $this->consumer = $consumer;
    }
}
