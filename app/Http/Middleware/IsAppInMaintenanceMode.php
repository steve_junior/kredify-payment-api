<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Transformers\JsonStructure;
use Illuminate\Support\Facades\App;

class IsAppInMaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (App::isDownForMaintenance() && ! is_whitelisted($request)) {
            return JsonStructure::error('Server is offline for maintenance', RESPONSE_SERVICE_UNAVAILABLE, 503);
        }

        return $next($request);
    }
}
