<?php

namespace App\Http\Controllers;

use App\Services\LogService;
use Illuminate\Support\Facades\DB;
use App\Transformers\JsonStructure;

class ServiceController extends Controller
{
    /*
     * Responds 'pong' if items in checklist passes
     * Checklist:
     *      1. Is the database online
     */
    public function ping()
    {
        $database_online = false;

        try {
            if (DB::connection()->getPdo()) {
                $database_online = true;
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_DatabaseConnectionError, $exception);
        }

        $response = $database_online ? 'pong' : '';

        if (is_empty($response)) {
            if (! $database_online) {
                return JsonStructure::error('Database is currently offline', RESPONSE_SERVICE_UNAVAILABLE, 503);
            }
        }

        return JsonStructure::success($response, 'Server is ready to accept requests');
    }
}
