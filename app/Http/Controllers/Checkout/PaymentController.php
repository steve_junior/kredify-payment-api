<?php

namespace App\Http\Controllers\Checkout;

use App\Services\LogService;
use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Services\ResponseService;
use App\Services\Payment\Paystack;
use App\Transformers\JsonStructure;
use App\Http\Controllers\Controller;
use App\Services\Checkout\CheckoutService;

class PaymentController extends Controller
{
    protected $paymentService;

    protected $checkoutService;

    protected $responseService;

    public function __construct(Paystack $paymentService, CheckoutService $checkoutService, ResponseService $responseService)
    {
        $this->paymentService = $paymentService;
        $this->checkoutService = $checkoutService;
        $this->responseService = $responseService;
    }

    /**
     * Trigger first payment using the schedule ID.
     *
     * @var
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getPaymentURL(Request $request)
    {
        $errors = $this->validatePaymentRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        $invoice = $this->checkoutService->createInvoiceFromSchedule($request->get(self::invoiceId()));

        $errors = $this->validateInvoice($invoice);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invoice error', RESPONSE_BAD_REQUEST, 400, $errors);
        }

        try {
            $payment = $this->paymentService->pay($invoice);

            if ($payment->provider_name == PayStack_NameSlug) {
                //get authorization URL
                if ($payment->response->status and isset($payment->response->data->authorization_url)) {
                    $data = $this->responseService->payStackAuthorisation($payment->response);

                    return JsonStructure::success($data);
                }

                return JsonStructure::error($payment->response->message, RESPONSE_OUTBOUND_SERVICE_UNAVAILABLE, 503);
            }
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_PaymentProviderError, $exception);
        }

        return JsonStructure::error('Error connecting to payment provider');
    }

    private static function invoiceId()
    {
        return 'invoiceId';
    }

    private static function checkoutToken()
    {
        return ScheduleController::checkoutToken();
    }

    private function validatePaymentRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::invoiceId(), static::checkoutToken()]),
            [
                static::invoiceId()      => 'required|string',
                static::checkoutToken()  => 'required|string|exists:checkouts,token',
            ]);
    }

    private function validateInvoice($invoice)
    {
        $errors = [];

        if (! $invoice) {
            array_push($errors, 'Invoice is either invalid or processed');
        }

        return $errors;
    }
}
