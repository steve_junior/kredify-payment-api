<?php

namespace App\Http\Controllers\Checkout;

use App\Models\Checkout;
use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Services\ResponseService;
use App\Transformers\JsonStructure;
use App\Http\Controllers\Controller;
use App\Services\Checkout\CheckoutService;

class ScheduleController extends Controller
{
    protected $checkoutService;

    protected $responseService;

    public function __construct(CheckoutService $checkoutService,
                                ResponseService $responseService)
    {
        $this->checkoutService = $checkoutService;
        $this->responseService = $responseService;
    }

    /*
     * Consumer Checkout Process
     *
     * first check the validity of the submitted token
     * the user will be taken through a slide show to complete their transaction.
     * after the account is created, and transaction is complete,
     * user make first payment using their approved method of payment.
     * then get notified of their payment schedules
     */

    public function getSchedules(Request $request)
    {
        $errors = $this->validateRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        $checkout = $this->checkoutService->getCheckoutByToken($request->get(self::checkoutToken()));

        $errors = $this->validateCheckout($checkout);
        if (! is_empty($errors)) {
            return JsonStructure::error('Token error', RESPONSE_BAD_REQUEST, 400, $errors);
        }

        $this->checkoutService->bindCheckoutToAuth($checkout);
        session()->put(Session_CurrentCheckoutToken, $request->get(self::checkoutToken()));

        $schedules = $this->checkoutService->getOrCreateSchedules($checkout);

        $response = $this->responseService->createCheckout($checkout, $schedules);

        return JsonStructure::success($response);
    }

    public static function checkoutToken()
    {
        return 'token';
    }

    private function validateRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::checkoutToken()]),
            [
                static::checkoutToken()  => 'required|string',
            ]);
    }

    private function validateCheckout($checkout)
    {
        $errors = [];

        if (! ($checkout instanceof  Checkout)) {
            array_push($errors, 'Invalid token');
        } else {
            if ($checkout->hasExpired()) {
                array_push($errors, 'Token has expired');
            }

            if ($checkout->hasOwner() && ! $checkout->belongsToLoggedInConsumer()) {
                //loggedIn user trying to checkout another consumer's checkout token
                array_push($errors, 'Token used by another consumer');
            }
        }

        return $errors;
    }
}
