<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 26/12/2020
 * Time: 9:02 PM.
 */

namespace App\Http\Controllers\Checkout;

use App\Services\LogService;
use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Services\Payment\Paystack;
use App\Events\PaidThroughPayStack;
use App\Http\Controllers\Controller;
use App\Repositories\PaymentEventRepository;
use App\Models\Payments\Event as PaymentEvent;

class CallbackController extends Controller
{
    protected $paymentService;

    public function __construct(Paystack $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * {PayStack Only}
     * Receives callback from payment provider.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paystack(Request $request)
    {
        $errors = $this->validatePaystackCallbackRequest($request);
        if (! is_empty($errors)) {
            abort(404);
        }

        $payload = init();
        $payload->reference = $request->get(self::paystackReference());

        try {
            $payment = $this->paymentService->validate($payload);

            if ($payment->response->status && $payment->response->data->status === 'success') {
                //payment was successful
                event(new PaidThroughPayStack($payment));
            }

            return $this->redirectConsumer($payment);
        } catch (\Exception $exception) {
            LogService::ErrorLog(Log_PaystackTransactionVerificationError, $exception);
        }

        return redirect('https://kredify.net');
    }

    private static function paystackReference()
    {
        return 'reference';
    }

    private static function paystackTrxRef()
    {
        return 'trxref';
    }

    private function validatePaystackCallbackRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::paystackReference(), static::paystackTrxRef()]),
            [
                static::paystackReference()  => 'required|string',
                static::paystackTrxRef()     => 'required|string|same:'.self::paystackReference(),
            ]);
    }

    private function redirectConsumer(Paystack $payment)
    {
        $response_data = $payment->response->data;
        $status = $response_data->status;
        $paymentEvent = (new PaymentEventRepository())->findOne(['reference_id' => $response_data->reference]);
        if ($paymentEvent instanceof PaymentEvent) {
            $order = $paymentEvent->checkout->relatedOrder;

            if ($status == PAYMENT_SUCCESS) {
                $destination = $order->confirmation_url;
            } elseif ($status == PAYMENT_FAILED) {
                $destination = $order->cancellation_url;
            } else {
                $destination = $paymentEvent->checkout->full_link;
            }

            return redirect()->to($destination);
        }

        return back();
    }
}
