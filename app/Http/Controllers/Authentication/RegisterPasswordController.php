<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 7:15 PM.
 */

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Models\Consumer\Consumer;
use App\Transformers\JsonStructure;
use Illuminate\Support\Facades\Hash;

class RegisterPasswordController extends AuthController
{
    public function action(Request $request)
    {
        $errors = $this->validateRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        $consumer = $this->consumers->getConsumerByRegistrationId($request->get(static::regId()));

        if (is_null($consumer)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422);
        }

        if ($consumer instanceof Consumer) {
            $consumer = $this->consumers->getUpdatedConsumer($consumer, ['password' => Hash::make($request->get(static::password()))]);

            $this->checkListScan($consumer);

            return $this->sendResponse($consumer);
        }

        return JsonStructure::error('Duplicate regId found. Contact support');
    }

    private function sendResponse(Consumer $consumer)
    {
        $data = $this->responseService->initialisedScan($consumer, $this->checkList);

        return JsonStructure::success($data);
    }

    private function validateRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::password(), static::regId()]),
            [
                static::regId()       => 'required|string',
                static::password()    => 'required',
            ]);
    }
}
