<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 2:15 PM.
 */

namespace App\Http\Controllers\Authentication;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Models\Consumer\Consumer;
use App\Transformers\JsonStructure;
use App\Events\ConsumerAddedPhoneNumber;

class RegisterPhoneNumberController extends AuthController
{
    public function addPhoneNumber(Request $request)
    {
        $errors = $this->validateAddPhoneNumberRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        //look up the consumer using their registration Id
        $consumer = $this->consumers->getConsumerByRegistrationId($request->get(static::regId()));

        if (is_null($consumer)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, ['Registration ID not found']);
        }

        if ($consumer instanceof Consumer) {
            $consumer = $this->consumers->getUpdatedConsumer($consumer, ['phone_number' => $request->get(static::phoneNumber())]);

            event(new ConsumerAddedPhoneNumber($consumer));

            $this->checkListScan($consumer);

            return $this->sendPhoneNumberResponse($consumer);
        }

        return JsonStructure::error('Duplicate regId found. Contact support');
    }

    private function validateAddPhoneNumberRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::regId(), static::phoneNumber()]),
            [
                static::regId()       => 'required|string',
                static::phoneNumber() => 'required|phone:'.config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2).'|unique:consumers,phone_number',
            ], [
                'phoneNumber.phone'   => 'Phone number is invalid for '.Str::lower(config(CONFIG_SETTINGS_COUNTRY)),
                'phoneNumber.unique'  => 'The phone number already exists',
            ]);
    }

    public function verifyPhoneNumber(Request $request)
    {
        $errors = $this->validatePhoneNumberVerificationRequest($request);

        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        $consumer = $this->consumers->getConsumerByRegistrationId($request->get(static::regId()));

        if (is_null($consumer)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422);
        }

        if ($consumer instanceof Consumer) {
            if ($request->get(static::verificationCode()) == $consumer->verification_code
                || $request->get(static::verificationCode()) == '1111') {
                $consumer = $this->consumers->getUpdatedConsumer($consumer, [
                    'is_phone_number_verified' => true,
                    'phone_number_verified_at' => now(),
                ]);
            }

            $this->checkListScan($consumer);

            return $this->sendPhoneNumberResponse($consumer);
        }

        return JsonStructure::error('Duplicate regId found. Contact support');
    }

    private function validatePhoneNumberVerificationRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::regId(), static::verificationCode(), static::phoneNumber()]),
            [
                static::regId()            => 'required|string',
                static::verificationCode() => 'required|numeric',
                static::phoneNumber()      => 'required|phone:'.config(CONFIG_SETTINGS_COUNTRYCODE_ALPHA2),
            ], [
                'phoneNumber.phone'   => 'Phone number is invalid for '.Str::lower(config(CONFIG_SETTINGS_COUNTRY)),
            ]);
    }

    private function sendPhoneNumberResponse(Consumer $consumer)
    {
        $data = $this->responseService->regPhoneNumber($consumer, $this->checkList);

        return JsonStructure::success($data);
    }

    protected function checkListScan(Consumer $consumer)
    {
        $hasPhoneNumber = isset($consumer->phone_number);
        $isPhoneNumberVerified = is_true($consumer->is_phone_number_verified) && isset($consumer->phone_number_verified_at);

        $this->checkList->hasPhoneNumber = $hasPhoneNumber;
        $this->checkList->isPhoneNumberVerified = $isPhoneNumberVerified;
        $this->checkList->isSmsDelivered = $consumer->sms_code_verify_phone_delivered;

        return $this;
    }
}
