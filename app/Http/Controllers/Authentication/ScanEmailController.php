<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 22/12/2020
 * Time: 7:32 PM.
 */

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Models\Consumer\Consumer;
use App\Transformers\JsonStructure;
use Illuminate\Auth\Events\Registered;

class ScanEmailController extends AuthController
{
    public function action(Request $request)
    {
        $errors = $this->getValidationError($request);

        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        //lookup the email in consumers table
        $consumer = $this->consumers->getConsumerByEmail($request->get(static::username()));

        if (is_null($consumer)) {
            // create the consumer
            $consumer = $this->consumers->create($request->only(static::username()));
            event(new Registered($consumer));
        }

        if ($consumer instanceof Consumer) {
            $this->checkListScan($consumer);

            return $this->sendScanResponse($consumer);
        }

        return JsonStructure::error('Duplicate email found. Contact support');
    }

    private function sendScanResponse(Consumer $consumer)
    {
        $data = $this->responseService->initialisedScan($consumer, $this->checkList);

        if ($data->isNew) {
            $message = 'Consumer created';
            $http_code = 201;
        } else {
            $message = 'Consumer retrieved';
            $http_code = 200;
        }

        return JsonStructure::success($data, $message, $http_code);
    }

    private function getValidationError(Request $request)
    {
        return RequestService::getValidationError($request->only(static::username()), [
            static::username() => 'required|string|email',
//            static::username() => 'required|string|email:rfc,dns,strict,spoof'
        ]);
    }
}
