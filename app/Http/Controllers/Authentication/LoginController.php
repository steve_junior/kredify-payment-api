<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 25/12/2020
 * Time: 6:04 AM.
 */

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Transformers\JsonStructure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginController extends AuthController
{
    use ThrottlesLogins;

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $errors = $this->validateLoginRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        // We using ThrottlesLogins trait to automatically throttle
        // the login attempts for this backend. The username will be paired
        // to IP address of the client making these requests into the backend.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        $error = [
                [Lang::get('auth.throttle',
                    [
                    'seconds' => $seconds,
                    'minutes' => ceil($seconds / 60),
                    ]
                )],
        ];

        return JsonStructure::error('Too many login attempts', RESPONSE_FAILED, 429, $error);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $error = [
            [trans('auth.failed')],
        ];

        return JsonStructure::error('Login failed', null, 401, $error);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $consumer = $this->consumers->getUpdatedConsumer($this->guard()->user(), ['recent_login' => now()]);

        return JsonStructure::success(init(), 'Authenticated', 200);
    }

    protected function validateLoginRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::username(), static::password()]),
            [
                static::username() => 'required|string|email',
//                static::username() => 'required|string|email:rfc,dns,strict,spoof',
                static::password() => 'required|string',
            ]);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(static::username(), static::password());
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        $this->loggedOut($request);

        return JsonStructure::success(init(), 'Exited', 200);
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }
}
