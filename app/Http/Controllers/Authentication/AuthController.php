<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 24/12/2020
 * Time: 8:02 AM.
 */

namespace App\Http\Controllers\Authentication;

use App\Models\Consumer\Consumer;
use App\Services\ResponseService;
use App\Http\Controllers\Controller;
use App\Repositories\ConsumerRepository;

class AuthController extends Controller
{
    protected $consumers;

    protected $responseService;

    protected $checkList;

    public function __construct(ConsumerRepository $repository, ResponseService $responseService)
    {
        $this->consumers = $repository;
        $this->responseService = $responseService;
        $this->checkList = init();
    }

    protected static function username()
    {
        return 'email';
    }

    protected static function phoneNumber()
    {
        return 'phoneNumber';
    }

    protected static function password()
    {
        return 'password';
    }

    protected static function regId()
    {
        return 'registrationId';
    }

    protected static function verificationCode()
    {
        return 'verificationCode';
    }

    protected static function legalFirstName()
    {
        return 'legalFirstName';
    }

    protected static function legalLastName()
    {
        return 'legalLastName';
    }

    protected static function legalOtherNames()
    {
        return 'legalOtherNames';
    }

    protected static function gAddress()
    {
        return 'gMapAddress';
    }

    protected static function region()
    {
        return 'region';
    }

    protected static function landmark()
    {
        return 'landmark';
    }

    protected function checkListScan(Consumer $consumer)
    {
        $hasPhoneNumber = isset($consumer->phone_number);
        $isPhoneNumberVerified = is_true($consumer->is_phone_number_verified) && isset($consumer->phone_number_verified_at);
        $hasPasswordSet = isset($consumer->password);
        $hasNameSet = isset($consumer->first_name) && isset($consumer->last_name);
        $hasAddressSet = isset($consumer->residential_address_google_maps);

        $this->checkList->hasPhoneNumber = $hasPhoneNumber;
        $this->checkList->hasPassword = $hasPasswordSet;
        $this->checkList->isPhoneNumberVerified = $isPhoneNumberVerified;
        $this->checkList->hasLegalName = $hasNameSet;
        $this->checkList->hasAddress = $hasAddressSet;

        $this->checkList->allChecksPassed = $hasPhoneNumber
                                            && $isPhoneNumberVerified
                                            && $hasPasswordSet
                                            && $hasNameSet
                                            && $hasAddressSet;

        return $this;
    }
}
