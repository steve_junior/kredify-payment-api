<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/12/2020
 * Time: 1:27 PM.
 */

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Services\RequestService;
use App\Models\Consumer\Consumer;
use App\Transformers\JsonStructure;

class RegisterNameAndAddressController extends AuthController
{
    public function action(Request $request)
    {
        $errors = $this->validateRequest($request);
        if (! is_empty($errors)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422, $errors);
        }

        $consumer = $this->consumers->getConsumerByRegistrationId($request->get(static::regId()));

        if (is_null($consumer)) {
            return JsonStructure::error('Invalid parameter', RESPONSE_BAD_REQUEST, 422);
        }

        if ($consumer instanceof Consumer) {
            $consumer = $this->consumers->getUpdatedConsumer($consumer, [
                'first_name'  => $request->get(static::legalFirstName()),
                'last_name'   => $request->get(static::legalLastName()),
                'other_names' => $request->get(static::legalOtherNames()),
                'residential_address_google_maps'  => $request->get(static::gAddress()),
                'region'      => $request->get(static::region()),
                'landmarks'   => $request->get(static::landmark()),
            ]);

            $this->checkListScan($consumer);

            return $this->sendResponse($consumer);
        }

        return JsonStructure::error('Duplicate regId found. Contact support');
    }

    private function sendResponse(Consumer $consumer)
    {
        $data = $this->responseService->initialisedScan($consumer, $this->checkList);

        return JsonStructure::success($data);
    }

    private function validateRequest(Request $request)
    {
        return RequestService::getValidationError(
            $request->only([static::regId(), static::legalFirstName(), static::legalLastName(), static::legalOtherNames(), static::gAddress(), static::region(), static::landmark()]),
            [
                static::regId()             => 'required|string',
                static::legalFirstName()    => 'required|string',
                static::legalLastName()     => 'required|string',
                static::legalOtherNames()   => 'string',
                static::gAddress()          => 'required|string',
                static::region()            => 'string',
                static::landmark()          => 'string',
            ]);
    }
}
