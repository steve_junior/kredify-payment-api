<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 28/11/2020
 * Time: 1:18 PM.
 */

namespace App\Http\Controllers;

use App\Models\Payments\Provider as PaymentProvider;

class TestController extends Controller
{
    public function test()
    {
        return abort(500);
    }

    public function getPaymentProvider()
    {
        return PaymentProvider::all()->first();
    }
}
