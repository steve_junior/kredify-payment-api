<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\Consumer\Consumer;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $this->authenticated($this->guard()->user());

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect()->intended($this->redirectTo($request));
    }

    protected function authenticated($user)
    {
        if ($user instanceof Consumer) {
//            $user->updateAttributes(['recent_login' => now()]);
        }
    }

    protected function redirectTo(Request $request)
    {
        $intended_url = $request->session()->get('url.intended');

        if (isset($intended_url)) {
            return $intended_url;
        }

        return RouteServiceProvider::HOME;
    }
}
