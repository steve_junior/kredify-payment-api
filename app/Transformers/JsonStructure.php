<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 26/11/2020
 * Time: 8:58 PM.
 */

namespace App\Transformers;

use stdClass;

class JsonStructure
{
    public static function error($message = '', $status = null, $http_code = null, $data = null)
    {
        $status = is_null($status) ? RESPONSE_FAILED : $status;
        $http_code = is_null($http_code) ? 500 : $http_code;
        $message = is_empty($message) ? 'Generic Error' : $message;

        return self::getResponse($status, $http_code, $message, $data);
    }

    public static function success($data = null, $message = '', $http_code = null)
    {
        $http_code = is_null($http_code) ? 200 : $http_code;
        $message = is_empty($message) ? 'Operation successful' : $message;

        return self::getResponse(RESPONSE_SUCCESS, $http_code, $message, $data);
    }

    /**
     * @param $status
     * @param $http_code
     * @param $message
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    private static function getResponse($status, $http_code, $message, $data = null)
    {
        $response = init();

        $error = init();

        $error->status = in_array($status, getHttpMessages()) ? $status : '';
        $error->code = in_array($http_code, getHttpCodes()) ? $http_code : new \Exception('Unidentified HTTP Code');
        $error->msg = is_null($message) ? '' : $message;

        if (in_array($http_code, getSuccessHttpCodes())) {
            $error->data = init();

            if ($data instanceof StdClass || is_array($data) || is_string($data)) {
                $response->data = $data;
            } else {
                $response->data = null;
            }
        } else {
            $error->data = is_null($data) ? init() : $data;
            $response->data = init();
        }

        $response->error = $error;

        return response()->json($response, $http_code);
    }
}
