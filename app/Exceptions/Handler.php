<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Support\Arr;
use App\Services\LogService;
use App\Transformers\JsonStructure;
use Illuminate\Database\QueryException;
use Dotenv\Exception\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable $exception
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        $this->reportExceptionsAsCritical($exception);

        parent::report($exception);
    }

    /*
     * Exceptions that are reported as critical and sent
     * to Slack channel
     */
    protected function reportExceptionsAsCritical(\Exception $e)
    {
        $excludedExceptions = [
            AuthenticationException::class,
            TokenMismatchException::class,
            NotFoundHttpException::class,
            ValidationException::class,
            MethodNotAllowedException::class,
        ];

        $shouldReport = is_null(Arr::first($excludedExceptions, function ($type) use ($e) {
            return $e instanceof $type;
        }));

        if ($shouldReport) {
            LogService::CriticalLog(Log_AppExceptionHandler, $e);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof QueryException) {
            return JsonStructure::error(__('Database connection error'));
        } elseif ($exception instanceof NotFoundHttpException) {
            return JsonStructure::error(__('Resource not found'), RESPONSE_FAILED, 404);
        } elseif ($exception instanceof MethodNotAllowedException) {
            return JsonStructure::error(__('Method not allowed on route action'), RESPONSE_FAILED, 405);
        } elseif ($exception instanceof AuthenticationException) {
            return JsonStructure::error(__('Login to continue'), RESPONSE_UNAUTHENTICATED, 401);
        }

        return JsonStructure::error(__('Server error'), RESPONSE_FAILED, 500);

//        return parent::render($request, $exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param AuthenticationException $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return JsonStructure::error(__('Login to continue'), RESPONSE_UNAUTHENTICATED, 401);
    }
}
