<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('other_names')->nullable();
            $table->timestamp('date_of_birth')->nullable();

            $table->timestamp('recent_login')->nullable();

            $table->string('email')->unique()->index();
            $table->string('phone_number')->unique()->index()->nullable();

            $table->string('password')->nullable();
            $table->text('two_factor_secret')->nullable();
            $table->text('two_factor_recovery_codes')->nullable();
            $table->string('remember_token', 100)->nullable();

            $table->boolean('is_email_verified')->default(false);
            $table->timestamp('email_verified_at')->nullable();

            $table->boolean('is_phone_number_verified')->default(false);
            $table->timestamp('phone_number_verified_at')->nullable();
            $table->string('phone_number_verification_code', 10)->nullable();

            $table->text('residential_address_google_maps')->nullable();
            $table->string('region')->nullable();
            $table->string('landmarks')->nullable();

            $table->string('registration_id')->unique()->index()->nullable();

            $table->boolean('sms_code_verify_phone_delivered')->default(false);
            $table->timestamp('sms_code_verify_phone_delivered_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['consumers'];

        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
    }
}
