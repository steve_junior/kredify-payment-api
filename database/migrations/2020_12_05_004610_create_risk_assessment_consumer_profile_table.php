<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskAssessmentConsumerProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('level');
            $table->unsignedSmallInteger('factor');
            $table->unsignedSmallInteger('assessment_score');
            $table->unsignedSmallInteger('allowed_intervals_in_days');
            $table->string('allowed_payout_ratio');
            $table->timestamp('assessed_at')->nullable();
            $table->json('assessment')->nullable();
            $table->json('attributes')->nullable();

            $table->unsignedBigInteger('consumer_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_profiles');
    }
}
