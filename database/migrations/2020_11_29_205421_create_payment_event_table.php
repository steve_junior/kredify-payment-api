<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_providers', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->string('slug')->unique();
            $table->json('configuration')->nullable();
            $table->timestamps();
        });

        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_default')->default(false);
            $table->boolean('is_card')->default(false);

            $table->json('data')->nullable();
            $table->string('unique_attribute')->index()->nullable();
            $table->timestamp('last_used')->nullable();

            $table->string('payment_provider_id')->index();
            $table->unsignedBigInteger('consumer_id')->index();
            $table->timestamps();
        });

        Schema::create('payment_events', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->json('amount');
            $table->unsignedBigInteger('reference_id')->index();
            $table->enum('status', [PAYMENT_STATUSES])->default(PAYMENT_INITIATED);

            $table->timestamp('attempted_at')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->unsignedBigInteger('checkout_id')->index();

            $table->unsignedBigInteger('payment_method_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_events');
        Schema::dropIfExists('payment_methods');
        Schema::dropIfExists('payment_providers');
    }
}
