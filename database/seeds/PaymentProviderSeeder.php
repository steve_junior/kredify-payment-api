<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payments\Provider as PaymentProvider;

class PaymentProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentProvider::create([
            'name' => 'PayStack Limited',
            'configuration' => [
                'country'            => config(CONFIG_SETTINGS_COUNTRY),
                'allowed_channels'   => ['card', 'mobile_money'],
                'preferred_channels' => ['card'],
                'currency'         => config(CONFIG_SETTINGS_CURRENCY),
                'api_endpoints'    => [
                    'initialise'   => 'https://api.paystack.co/transaction/initialize',
                    'verify'       => 'https://api.paystack.co/transaction/verify',
                    'charge_authorization' => 'https://api.paystack.co/transaction/charge_authorization',
                ],
            ],
        ]);
    }
}
