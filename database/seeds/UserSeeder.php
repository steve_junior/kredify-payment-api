<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Consumer\Consumer;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $consumer = new Consumer();

        $consumer->first_name = 'Julius';
        $consumer->last_name = 'Afriyie';
        $consumer->other_names = 'Nana Kwame';
        $consumer->phone_number = '+233552515858';
        $consumer->email = 'test@gmail.com';
        $consumer->password = bcrypt('secret');
        $consumer->is_email_verified = true;
        $consumer->is_phone_number_verified = true;
        $consumer->region = 'Greater Accra';
        $consumer->landmarks = 'Flag Staff House';

        $consumer->save();
    }
}
